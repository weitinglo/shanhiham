const express = require('express')
const app = express()
const port = 3000
// const sqlite3 = require('sqlite3').verbose();
const Database = require('better-sqlite3');
const db = new Database('sanhihamDB.db', {});
db.pragma('journal_mode = WAL');

const ecpay_payment = require('ecpay_payment_nodejs');

var nodemailer = require('nodemailer');
const bcrypt = require('bcryptjs');
var bodyParser = require('body-parser');
const session = require('express-session')
const FileStore = require('session-file-store')(session);
var fs = require('fs');


app.use(bodyParser.json({ limit: '10mb' }));
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }));


app.use(express.static(__dirname + '/assets'));
app.use(express.static(__dirname + '/view'));

// app.use(express.static('/home/weitinglo/Documents/Projects/Web/ham/assets'));
// app.use(express.static('/home/weitinglo/Documents/Projects/Web/ham/view'));


/* create session obj */
app.use(session({
    name: 'session-id',
    secret: 'sanhihamham-web',
    store: new FileStore(),
    saveUninitialized: true,
    resave: false,
    cookie: { maxAge: 1200 * 1000 } //20mins 
}))

/* Application landing page - index.html */
app.get('/', function (req, res) {
    res.sendFile(__dirname + "/view/" + "index.html");
});

app.get('/about.html', function (req, res) {
    res.redirect('/');

});
app.get('/shop.php', function (req, res) {
    res.redirect('/');
});


/* check whether the session is alive. If it is then extend the session time */
var checkLogin = function (req) {
    if (req.session && req.session.loggedin) {
        extendSession(req);
        return true;
    }
    return false;
}

/* Check whether the user is logged in or his/her session is still active */
app.get('/check_login', function (req, res) {
    let jsonMsg = {
        login: false,
        level: -1
    }
    try {
        jsonMsg.login = checkLogin(req);
        jsonMsg.level = req.session.userLevel;
    } catch (err) {
        console.log(err)
    }
    res.send(jsonMsg);
});




/**************************Login Required *************************/
/* Getting User data for session, profile info - need to check session  activity*/
app.get('/user_data', function (req, res) {
    let jsonMsg = {
        level: -1,
        name: "",
        email: "",
        id: -1,
        login: false
    }
    try {
        jsonMsg.login = checkLogin(req);
        if (jsonMsg.login) {
            jsonMsg.name = req.session.userName;
            jsonMsg.email = req.session.userEmail;
            jsonMsg.id = req.session.userId;
            jsonMsg.level = req.session.userLevel;

        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試",
            code: 500,
        }

        res.send(jsonMsg);
    }
    res.send(jsonMsg);
});

/* Admin User - Getting all the user information for the website ------ needs to have the admin level rights */
app.get('/user_list', function (req, res) {
    try {
        extendSession(req)
        if (checkLogin(req) && req.session.userLevel == 0) {
            const rows = db.prepare("SELECT * FROM USER").all();
            res.send(rows);
        } else {
            let jsonMsg = {
                msg: "您沒有權限喔",
                code: 500,
            }
            res.send(jsonMsg);
        }

    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試",
            code: 500,
        }
        res.send(jsonMsg);
    }

});

/* Checking the placed orders */
// app.get('/order', function (req, res) {
//     try {
//         let userId = req.session.userId;
//         if (checkLogin(req) && userId) {
//             const rows = db.prepare("SELECT PRODUCT_ORDER.id , PRODUCT_ORDER.product_id , PRODUCT_ORDER.product_price , PRODUCT_ORDER.product_amout , PRODUCT_ORDER.order_id ,PRODUCT_ORDER.date ,PRODUCT.name FROM PRODUCT_ORDER LEFT JOIN PRODUCT WHERE PRODUCT_ORDER.product_id=PRODUCT.id and PRODUCT_ORDER.USER_ID = ?").all(userId);
//             res.send(rows);
//         } else {
//             let jsonMsg = {
//                 msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
//                 code: 401,
//             }
//             res.send(jsonMsg);
//         }
//     } catch (err) {
//         let jsonMsg = {
//             msg: "系統出現問題，請稍候再試 -" + err.message,
//             code: 500,
//         }

//         res.send(jsonMsg);
//     }
// });

/* get product order list table on id */
app.get('/order_list_on_id', function (req, res) {
    try {
        let userId = req.session.userId;
        if (checkLogin(req) && userId) {
            let level = req.session.userLevel;
            let sql = "";
            if (level == 0) {
                let orderId = req.query.orderId;
                sql = 'SELECT * FROM PRODUCT_ORDER_LIST WHERE ORDER_ID = ?';
                const rows = db.prepare(sql).all(orderId);
                res.send(rows);
            } else {
                let jsonMsg = {
                    msg: "您沒有權限喔",
                    code: 500
                }
                res.send(jsonMsg);
            }
        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試",
            code: 500,
        }
        res.send(jsonMsg);
    }
});

/* get all the orders */
app.get('/order_list', function (req, res) {
    try {
        let userId = req.session.userId;
        if (checkLogin(req) && userId) {
            let level = req.session.userLevel;

            let sql = "";
            if (level == 0)
                sql = 'SELECT * FROM PRODUCT_ORDER_LIST';
            else
                sql = 'SELECT * FROM PRODUCT_ORDER_LIST WHERE USER_ID = ' + userId;
            const rows = db.prepare(sql).all();
            res.send(rows);
        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試",
            code: 500,
        }
        res.send(jsonMsg);
    }
});

/* user get order details with produdct names and detail*/
app.get('/order_on_id', function (req, res) {
    try {
        let userId = req.session.userId;
        if (checkLogin(req) && userId) {
            let orderId = req.query.orderId;
            const orderRows = db.prepare('SELECT PRODUCT_ORDER.id , PRODUCT_ORDER.product_id , PRODUCT_ORDER.user_id ,PRODUCT_ORDER.product_price ,  PRODUCT_ORDER.product_amout , PRODUCT_ORDER.order_id ,PRODUCT_ORDER.date ,PRODUCT.name, PRODUCT.frozen FROM PRODUCT_ORDER LEFT JOIN PRODUCT WHERE PRODUCT_ORDER.product_id=PRODUCT.id AND PRODUCT_ORDER.order_id=?').all(orderId);
            let userId = orderRows.length > 0 ? orderRows[0].user_id : orderRows.user_id;
            const userrows = db.prepare('SELECT * FROM USER WHERE ID =?').get(userId);
            let completedData = orderRows;
            for (let i = 0; i < orderRows.length; i++) {
                if (userrows.id == orderRows[i].user_id) {
                    completedData[i].username = userrows.name;
                }
            }
            res.send(completedData);
        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試-" + err.message,
            code: 500,
        }
        res.send(jsonMsg);
    }
});


/* get rules */
app.get('/rules', function (req, res) {
    try {
        let userId = req.session.userId;
        if (checkLogin(req) && userId) {
            const rows = db.prepare('SELECT * FROM RULES').get();
            res.send(rows);
        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試-" + err.message,
            code: 500,
        }

        res.send(jsonMsg);
    }
});

/* get the user profile*/
app.get('/profile', function (req, res) {
    try {
        let userId = req.session.userId;
        if (checkLogin(req) && userId) {
            const rows = db.prepare("SELECT * FROM USER WHERE ID ==?").get(userId);
            res.send(rows);
        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試",
            code: 500,
        }

        res.send(jsonMsg);
    }
});


/*消息狀態更新*/
app.post('/update_news_show', function (req, res) {
    try {
        extendSession(req)
        let jsonMsg = {};
        let id = req.body.id;
        let status = req.body.status;
        let show = status == true ? 1 : 0;
        let data = [show, id];
        let userId = req.session.userId;
        let level = req.session.userLevel;
        if (level != 0) {
            let jsonMsg = {
                msg: "您沒有權限喔",
                code: 500
            }
            res.send(jsonMsg);
        }
        if (checkLogin(req) && userId) {
            const stmt = db.prepare('UPDATE NEWS SET SHOW = ? WHERE ID =?');

            const info = stmt.run(data);
            if (info.changes == 1) {
                jsonMsg.msg = "更新成功";
                jsonMsg.code = 200;
                res.send(jsonMsg);
            } else {
                jsonMsg.msg = "更新失敗";
                jsonMsg.code = 222;
                res.send(jsonMsg);
            }
        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試 - " + err.message,
            code: 500,
        }

        res.send(jsonMsg);
    }
});


/*消息更新*/
app.post('/update_news', function (req, res) {
    try {
        extendSession(req)
        let jsonMsg = {};

        let userId = req.session.userId;
        let level = req.session.userLevel;
        if (level != 0) {
            let jsonMsg = {
                msg: "您沒有權限喔",
                code: 500
            }
            res.send(jsonMsg);
        }
        if (checkLogin(req) && userId) {
            let news = req.body.news;
            let data = [news.title, news.message, news.date, news.link, news.id];
            const stmt = db.prepare('UPDATE NEWS SET TITLE = ?, MESSAGE=?, DATE=?, LINK=? WHERE ID =?');
            const info = stmt.run(data);
            if (info.changes == 1) {
                jsonMsg.msg = "更新成功";
                jsonMsg.code = 200;
                res.send(jsonMsg);
            } else {
                jsonMsg.msg = "更新失敗";
                jsonMsg.code = 222;
                res.send(jsonMsg);
            }
        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試 - " + err.message,
            code: 500,
        }

        res.send(jsonMsg);
    }
});

/*更新社群*/
app.post('/update_social', function (req, res) {
    try {
        extendSession(req)
        let jsonMsg = {};

        let userId = req.session.userId;
        let level = req.session.userLevel;
        if (level != 0) {
            let jsonMsg = {
                msg: "您沒有權限喔",
                code: 500
            }
            res.send(jsonMsg);
        }
        if (checkLogin(req) && userId) {
            let social = req.body.social;
            let fail = false;
            for (let i = 0; i < social.length; i++) {
                let socialData = social[i];
                let data = [socialData.link, socialData.show, socialData.id];
                const stmt = db.prepare('UPDATE SOCIAL SET LINK = ?, SHOW=? WHERE ID =?');
                const info = stmt.run(data);
                if (info.changes == 0) {
                    fail = true;
                }
            }

            if (!fail) {
                jsonMsg.msg = "更新成功";
                jsonMsg.code = 200;
                res.send(jsonMsg);
            } else {
                jsonMsg.msg = "更新失敗";
                jsonMsg.code = 222;
                res.send(jsonMsg);
            }
        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試 - " + err.message,
            code: 500,
        }

        res.send(jsonMsg);
    }
});



/*出貨狀態更新*/
app.post('/update_order_status', function (req, res) {
    try {
        extendSession(req)
        let jsonMsg = {};
        let id = req.body.id;
        let status = req.body.status;
        let data = [status, id];
        let userId = req.session.userId;
        let level = req.session.userLevel;
        if (level != 0) {
            let jsonMsg = {
                msg: "您沒有權限喔",
                code: 500
            }
            res.send(jsonMsg);
        }
        if (checkLogin(req) && userId) {
            const stmt = db.prepare('UPDATE PRODUCT_ORDER_LIST SET STATUS = ? WHERE ID =?');
            const info = stmt.run(data);
            if (info.changes == 1) {
                jsonMsg.msg = "更新成功";
                jsonMsg.code = 200;
                res.send(jsonMsg);
            } else {
                jsonMsg.msg = "更新失敗";
                jsonMsg.code = 222;
                res.send(jsonMsg);
            }
        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試 - " + err.message,
            code: 500,
        }

        res.send(jsonMsg);
    }
});

/* Admin user - 看留言 */
app.get('/comment', function (req, res) {
    try {
        let level = req.session.userLevel;
        if (level != 0) {
            let jsonMsg = {
                msg: "您沒有權限喔",
                code: 500
            }
            res.send(jsonMsg);
        }
        if (checkLogin(req) && level == 0) {
            const rows = db.prepare('SELECT * FROM COMMENT').all();
            res.send(rows)
        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試-" + err.message,
            code: 500,
        }

        res.send(jsonMsg);
    }
});

/* Admin user - 送點數規則 */
app.get('/credits', function (req, res) {
    try {
        let level = req.session.userLevel;
        if (level != 0) {
            let jsonMsg = {
                msg: "您沒有權限喔",
                code: 500
            }
            res.send(jsonMsg);
        }
        if (checkLogin(req) && level == 0) {
            const rows = db.prepare('SELECT * FROM CREDITS').all();
            res.send(rows)
        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試-" + err.message,
            code: 500,
        }

        res.send(jsonMsg);
    }
});

/* Admin user - 送點數規則 */
app.get('/user_by_id', function (req, res) {
    try {
        let level = req.session.userLevel;
        if (level != 0) {
            let jsonMsg = {
                msg: "您沒有權限喔",
                code: 500
            }
            res.send(jsonMsg);
        }
        if (checkLogin(req) && level == 0) {
            let userId = req.query.userId;

            const rows = db.prepare('SELECT * FROM USER WHERE ID = ?').all(userId);
            res.send(rows)
        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試-" + err.message,
            code: 500,
        }

        res.send(jsonMsg);
    }
});
/*Admin User - 更新點數*/
app.post('/update_credit', function (req, res) {
    try {

        let jsonMsg = {};
        let credit = req.body.credit;
        let data = [credit.birthday_credit, credit.birthday_credit_enable, credit.id];
        let sql = 'UPDATE CREDITS SET BIRTHDAY_CREDIT = ? , BIRTHDAY_CREDIT_ENABLE = ? WHERE ID =?';
        if (req.session.userLevel != 0) {
            let jsonMsg = {
                msg: "您沒有權限",
                code: 500,
            }
            res.send(jsonMsg);
            return;
        }

        if (checkLogin(req) && req.session.userLevel == 0) {
            const stmt = db.prepare(sql);
            const info = stmt.run(data);
            if (info.changes >= 1) {
                jsonMsg.msg = "更新成功";
                jsonMsg.code = 200;
                res.send(jsonMsg);
            } else {
                jsonMsg.msg = "更新失敗";
                jsonMsg.code = 222;
                res.send(jsonMsg);
            }
        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試-" + err,
            code: 500,
        }
        res.send(jsonMsg);
    }
});

/* 刪除特定訂單 */
app.delete('/delete_order', function (req, res) {
    let jsonMsg = {};
    try {
        extendSession(req)
        let orderID = req.query.id;
        if (checkLogin(req) && req.session.userLevel == 0) {
            const stmt = db.prepare('DELETE FROM PRODUCT_ORDER WHERE ORDER_ID = ?');
            let data = [orderID];
            let dbInfo = stmt.run(data);
            const stmt2 = db.prepare('DELETE FROM PRODUCT_ORDER_LIST WHERE ORDER_ID = ?');
            let dbInfo2 = stmt2.run(data);

            if (dbInfo.changes > 0 && dbInfo2.changes > 0) {
                jsonMsg.msg = "刪除成功";
                jsonMsg.code = 200;
                res.send(jsonMsg);
            } else {
                jsonMsg.msg = "刪除失敗";
                jsonMsg.code = 222;
                res.send(jsonMsg);
            }
        } else {
            let jsonMsg = {
                msg: "您沒有權限",
                code: 500,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試 - " + err.message,
            code: 500,
        }
        res.send(jsonMsg);
    }
});

/* 刪除特定消息 */
app.delete('/delete_news', function (req, res) {
    let jsonMsg = {};
    try {
        extendSession(req)
        let orderID = req.query.id;
        if (checkLogin(req) && req.session.userLevel == 0) {
            const stmt = db.prepare('DELETE FROM NEWS WHERE ID = ?');
            let data = [orderID];
            let dbInfo = stmt.run(data);
            if (dbInfo.changes > 0) {
                jsonMsg.msg = "刪除成功";
                jsonMsg.code = 200;
                res.send(jsonMsg);
            } else {
                jsonMsg.msg = "刪除失敗";
                jsonMsg.code = 222;
                res.send(jsonMsg);
            }
        } else {
            let jsonMsg = {
                msg: "您沒有權限",
                code: 500,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試 - " + err.message,
            code: 500,
        }
        res.send(jsonMsg);
    }
});

/* 刪除特定媒體 */
app.delete('/delete_media', function (req, res) {
    let jsonMsg = {};
    try {
        extendSession(req)
        let orderID = req.query.id;
        if (checkLogin(req) && req.session.userLevel == 0) {
            const stmt = db.prepare('DELETE FROM MEDIA WHERE ID = ?');
            let data = [orderID];
            let dbInfo = stmt.run(data);
            if (dbInfo.changes > 0) {
                jsonMsg.msg = "刪除成功";
                jsonMsg.code = 200;
                res.send(jsonMsg);
            } else {
                jsonMsg.msg = "刪除失敗";
                jsonMsg.code = 222;
                res.send(jsonMsg);
            }
        } else {
            let jsonMsg = {
                msg: "您沒有權限",
                code: 500,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試 - " + err.message,
            code: 500,
        }
        res.send(jsonMsg);
    }
});

/* 刪除特定食譜 */
app.delete('/delete_recipe', function (req, res) {
    let jsonMsg = {};
    try {
        extendSession(req)
        let orderID = req.query.id;
        if (checkLogin(req) && req.session.userLevel == 0) {
            const stmt = db.prepare('DELETE FROM RECIPE WHERE ID = ?');
            let data = [orderID];
            let dbInfo = stmt.run(data);
            if (dbInfo.changes > 0) {
                jsonMsg.msg = "刪除成功";
                jsonMsg.code = 200;
                res.send(jsonMsg);
            } else {
                jsonMsg.msg = "刪除失敗";
                jsonMsg.code = 222;
                res.send(jsonMsg);
            }
        } else {
            let jsonMsg = {
                msg: "您沒有權限",
                code: 500,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試 - " + err.message,
            code: 500,
        }
        res.send(jsonMsg);
    }
});
/* 刪除特定產品 */
app.delete('/delete_product', function (req, res) {
    let jsonMsg = {};
    try {
        extendSession(req)
        let productId = req.query.id;
        if (checkLogin(req) && req.session.userLevel == 0) {
            const stmt = db.prepare('DELETE FROM PRODUCT WHERE ID = ?');
            let data = [productId];
            let dbInfo = stmt.run(data);
            if (dbInfo.changes == 1) {
                jsonMsg.msg = "刪除成功";
                jsonMsg.code = 200;
                res.send(jsonMsg);
            } else {
                jsonMsg.msg = "刪除失敗";
                jsonMsg.code = 222;
                res.send(jsonMsg);
            }
        } else {
            let jsonMsg = {
                msg: "您沒有權限",
                code: 500,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試 - " + err.message,
            code: 500,
        }
        res.send(jsonMsg);
    }
});


/* Admin media - 更新媒體 */
app.post('/update_menu', function (req, res) {
    try {
        extendSession(req)
        let jsonMsg = {};
       
        if (req.session.userLevel != 0) {
            let jsonMsg = {
                msg: "您沒有權限",
                code: 500,
            }
            res.send(jsonMsg);
            return;
        }
        if (checkLogin(req) && req.session.userLevel == 0) {
            let file = req.body.file;
            /* pic 1 */
            if (file) {                
                base64Data = file.replace("data:application/pdf;base64,", "");
                jsonMsg.msg = "更新成功";
                fs.writeFile("assets/download/menu.pdf", base64Data, { encoding: 'base64' }, function (err) {
                    if (err != null)
                        jsonMsg.msg = "上傳圖片有問題";
                });
               
            }
        
            jsonMsg.code = 200;
            res.send(jsonMsg);
           
        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試-" + err.message,
            code: 500,
        }

        res.send(jsonMsg);
    }
});


/* Admin media - 更新媒體 */
app.post('/update_media', function (req, res) {
    try {
        extendSession(req)
        let jsonMsg = {};
        let media = req.body.media;
        let data = [media.title, media.subtitle, media.description, media.show2, media.id];
        let sql = 'UPDATE MEDIA SET TITLE = ?, SUBTITLE = ? , DESCRIPTION = ? , SHOW2 = ? WHERE ID =?';
        if (req.session.userLevel != 0) {
            let jsonMsg = {
                msg: "您沒有權限",
                code: 500,
            }
            res.send(jsonMsg);
            return;
        }
        if (checkLogin(req) && req.session.userLevel == 0) {
            const stmt = db.prepare(sql);
            const info = stmt.run(data);
            if (info.changes == 1) {
                jsonMsg.msg = "更新成功";
                let imgPathJson = {
                    img1: media.img1,
                    img2: media.img2,
                    img3: media.img3,
                    img4: media.img4,
                    img5: media.img5,
                    img6: media.img6,
                    img7: media.img7,
                    img8: media.img8,
                    img9: media.img9,
                    img10: media.img10,
                    img11: media.img11,
                    img12: media.img12,
                    img13: media.img13,
                    img14: media.img14,
                    img15: media.img15
                }
                /* pic 1 */
                let img64 = media.newImg;
                if (img64) {
                    var extension = base64MimeType(img64); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img64.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img64.replace("data:image/png;base64,", "");
                    jsonMsg.msg = "更新成功";
                    if (media.img1) {
                        fs.writeFile("assets/" + media.img1, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {

                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                        imgPathJson.img1 = imgPath;
                    }
                }
                /* pic 2 */
                let img642 = media.newImg2;
                if (img642) {
                    var extension = base64MimeType(img642); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img642.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img642.replace("data:image/png;base64,", "");
                    jsonMsg.msg = "更新成功";
                    if (media.img2) {
                        fs.writeFile("assets/" + media.img2, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {
                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";

                        });
                        imgPathJson.img2 = imgPath;
                    }
                }
                /* pic 3 */
                let img643 = media.newImg3;
                if (img643) {
                    var extension = base64MimeType(img643); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img643.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img643.replace("data:image/png;base64,", "");
                    jsonMsg.msg = "更新成功";
                    if (media.img3) {
                        fs.writeFile("assets/" + media.img3, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {
                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";

                        });
                        imgPathJson.img3 = imgPath;
                    }
                }
                /* pic 4 */
                let img644 = media.newImg4;
                if (img644) {
                    var extension = base64MimeType(img644); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img644.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img644.replace("data:image/png;base64,", "");
                    jsonMsg.msg = "更新成功";
                    if (media.img4) {
                        fs.writeFile("assets/" + media.img4, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {
                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";

                        });
                        imgPathJson.img4 = imgPath;
                    }
                }
                /* pic 5 */
                let img645 = media.newImg5;
                if (img645) {
                    var extension = base64MimeType(img645); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img645.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img645.replace("data:image/png;base64,", "");
                    jsonMsg.msg = "更新成功";
                    if (media.img5) {
                        fs.writeFile("assets/" + media.img5, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {
                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";

                        });
                        imgPathJson.img5 = imgPath;
                    }
                }
                /* pic 6 */
                let img646 = media.newImg6;
                if (img646) {
                    var extension = base64MimeType(img646); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img646.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img646.replace("data:image/png;base64,", "");
                    jsonMsg.msg = "更新成功";
                    if (media.img6) {
                        fs.writeFile("assets/" + media.img6, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {
                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";

                        });
                        imgPathJson.img6 = imgPath;
                    }
                }
                /* pic 7 */
                let img647 = media.newImg7;
                if (img647) {
                    var extension = base64MimeType(img647); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img647.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img647.replace("data:image/png;base64,", "");
                    jsonMsg.msg = "更新成功";
                    if (media.img7) {
                        fs.writeFile("assets/" + media.img7, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {
                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";

                        });
                        imgPathJson.img7 = imgPath;
                    }
                }
                /* pic 8 */
                let img648 = media.newImg8;
                if (img648) {
                    var extension = base64MimeType(img648); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img648.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img648.replace("data:image/png;base64,", "");
                    jsonMsg.msg = "更新成功";
                    if (media.img8) {
                        fs.writeFile("assets/" + media.img8, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {
                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";

                        });
                        imgPathJson.img8 = imgPath;
                    }
                }
                /* pic 9 */
                let img649 = media.newImg9;
                if (img649) {
                    var extension = base64MimeType(img649); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img649.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img649.replace("data:image/png;base64,", "");
                    jsonMsg.msg = "更新成功";
                    if (media.img9) {
                        fs.writeFile("assets/" + media.img9, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {
                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";

                        });
                        imgPathJson.img9 = imgPath;
                    }
                }
                /* pic 10 */
                let img6410 = media.newImg10;
                if (img6410) {
                    var extension = base64MimeType(img6410); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img6410.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img6410.replace("data:image/png;base64,", "");

                    jsonMsg.msg = "更新成功";
                    if (media.img10) {
                        fs.writeFile("assets/" + media.img10, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {
                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";

                        });
                        imgPathJson.img10 = imgPath;
                    }
                }
                /* pic 11 */
                let img6411 = media.newImg11;
                if (img6411) {
                    var extension = base64MimeType(img6411); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img6411.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img6411.replace("data:image/png;base64,", "");

                    jsonMsg.msg = "更新成功";
                    if (media.img11) {
                        fs.writeFile("assets/" + media.img11, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {
                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";

                        });
                        imgPathJson.img11 = imgPath;
                    }
                }
                /* pic 12 */
                let img6412 = media.newImg12;
                if (img6412) {
                    var extension = base64MimeType(img6412); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img6412.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img6412.replace("data:image/png;base64,", "");

                    jsonMsg.msg = "更新成功";
                    if (media.img12) {
                        fs.writeFile("assets/" + media.img12, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {
                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";

                        });
                        imgPathJson.img12 = imgPath;
                    }
                }
                /* pic 13 */
                let img6413 = media.newImg13;
                if (img6413) {
                    var extension = base64MimeType(img6413); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img6413.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img6413.replace("data:image/png;base64,", "");
                    jsonMsg.msg = "更新成功";
                    if (media.img13) {
                        fs.writeFile("assets/" + media.img13, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {
                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";

                        });
                        imgPathJson.img13 = imgPath;
                    }
                }
                /* pic 14 */
                let img6414 = media.newImg14;
                if (img6414) {
                    var extension = base64MimeType(img6414); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img6414.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img6414.replace("data:image/png;base64,", "");
                    jsonMsg.msg = "更新成功";
                    if (media.img14) {
                        fs.writeFile("assets/" + media.img14, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {
                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";

                        });
                        imgPathJson.img14 = imgPath;
                    }
                }
                /* pic 15 */
                let img6415 = media.newImg15;
                if (img6415) {
                    var extension = base64MimeType(img6415); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img6415.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img6415.replace("data:image/png;base64,", "");
                    jsonMsg.msg = "更新成功";
                    if (media.img15) {
                        fs.writeFile("assets/" + media.img15, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {
                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";

                        });
                        imgPathJson.img15 = imgPath;
                    }
                }
                let sql_media = 'UPDATE MEDIA SET IMG1 = ?, IMG2 = ? , IMG3 = ? , IMG4 = ? , IMG5 = ? ,IMG6 = ?, IMG7 = ? , IMG8 = ? , IMG9 = ? , IMG10 = ?, IMG11 = ?, IMG12 = ? , IMG13 = ? , IMG14 = ? , IMG15 = ?  WHERE ID =?';
                let media_img_data = [imgPathJson.img1, imgPathJson.img2, imgPathJson.img3, imgPathJson.img4, imgPathJson.img5, imgPathJson.img6, imgPathJson.img7, imgPathJson.img8, imgPathJson.img9, imgPathJson.img10, imgPathJson.img11, imgPathJson.img12, imgPathJson.img13, imgPathJson.img14, imgPathJson.img15, media.id];

                const stmtImg = db.prepare(sql_media);
                const update_res = stmtImg.run(media_img_data);
                jsonMsg.code = 200;
                res.send(jsonMsg);
            } else {
                jsonMsg.msg = "更新失敗";
                jsonMsg.code = 222;
                res.send(jsonMsg);
            }

        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試-" + err.message,
            code: 500,
        }

        res.send(jsonMsg);
    }
});


/* Admin banner - 更新廣告 */
app.post('/update_banner', function (req, res) {
    try {
        extendSession(req)
        let jsonMsg = {};
        if (req.session.userLevel != 0) {
            let jsonMsg = {
                msg: "您沒有權限",
                code: 500,
            }
            res.send(jsonMsg);
            return;
        }
        if (checkLogin(req) && req.session.userLevel == 0) {
            let banner = req.body.banner;
            let imgPathJson = {
                desktop1:banner.desktop1,
                mobile1:banner.mobile1,
                desktop2:banner.desktop2,
                mobile2:banner.mobile2
            }
        
            /* pic 1 - desktop*/
            let img64 = banner.bannerImageDesktopSrc1;
            if (img64) {
                var extension = base64MimeType(img64); // "image/png"
                if (extension == 'jpeg' || extension == 'jpg') {
                    base64Data = img64.replace("data:image/jpeg;base64,", "");
                    extension = "jpg"
                }
                else
                    base64Data = img64.replace("data:image/png;base64,", "");
                jsonMsg.msg = "更新成功";
                if (banner.desktop1) {
                    fs.writeFile("assets/" + banner.desktop1, base64Data, { encoding: 'base64' }, function (err) {
                        if (err != null)
                            jsonMsg.msg = "上傳圖片有問題";
                    });
                } else {

                    let imgPath = "images/banner/" + _uuid() + "." + extension;
                    fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                        if (err != null)
                            jsonMsg.msg = "上傳圖片有問題";
                    });
                    imgPathJson.desktop1 = imgPath;
                }
            }
             /* pic 1 - mobile*/
             let img64_mobile = banner.bannerImageMobileSrc1;
             if (img64_mobile) {
                 var extension = base64MimeType(img64_mobile); // "image/png"
                 if (extension == 'jpeg' || extension == 'jpg') {
                     base64Data = img64_mobile.replace("data:image/jpeg;base64,", "");
                     extension = "jpg"
                 }
                 else
                     base64Data = img64_mobile.replace("data:image/png;base64,", "");
                 jsonMsg.msg = "更新成功";
                 if (banner.mobile1) {
                     fs.writeFile("assets/" + banner.mobile1, base64Data, { encoding: 'base64' }, function (err) {
                         if (err != null)
                             jsonMsg.msg = "上傳圖片有問題";
                     });
                 } else {
 
                     let imgPath = "images/banner/" + _uuid() + "." + extension;
                     fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                         if (err != null)
                             jsonMsg.msg = "上傳圖片有問題";
                     });
                     imgPathJson.mobile1 = imgPath;
                 }
             }
        
            /* pic 2 - desktop*/
            let img642 = banner.bannerImageDesktopSrc2;
            if (img642) {
                var extension = base64MimeType(img642); // "image/png"
                if (extension == 'jpeg' || extension == 'jpg') {
                    base64Data = img642.replace("data:image/jpeg;base64,", "");
                    extension = "jpg"
                }
                else
                    base64Data = img642.replace("data:image/png;base64,", "");
                jsonMsg.msg = "更新成功";
                if (banner.desktop2) {
                    fs.writeFile("assets/" + banner.desktop2, base64Data, { encoding: 'base64' }, function (err) {
                        if (err != null)
                            jsonMsg.msg = "上傳圖片有問題";
                    });
                } else {

                    let imgPath = "images/banner/" + _uuid() + "." + extension;
                    fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                        if (err != null)
                            jsonMsg.msg = "上傳圖片有問題";
                    });
                    imgPathJson.desktop2 = imgPath;
                }
            }
            /* pic 2 - mobile*/
            let img642_mobile = banner.bannerImageMobileSrc2;
            if (img642_mobile) {
                var extension = base64MimeType(img642_mobile); // "image/png"
                if (extension == 'jpeg' || extension == 'jpg') {
                    base64Data = img642_mobile.replace("data:image/jpeg;base64,", "");
                    extension = "jpg"
                }
                else
                    base64Data = img642_mobile.replace("data:image/png;base64,", "");
                jsonMsg.msg = "更新成功";
                if (banner.mobile2) {
                    fs.writeFile("assets/" + banner.mobile2, base64Data, { encoding: 'base64' }, function (err) {
                        if (err != null)
                            jsonMsg.msg = "上傳圖片有問題";
                    });
                } else {

                    let imgPath = "images/banner/" + _uuid() + "." + extension;
                    fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                        if (err != null)
                            jsonMsg.msg = "上傳圖片有問題";
                    });
                    imgPathJson.mobile2 = imgPath;
                }
            }
            let sql_media = 'UPDATE BANNER SET DESKTOP1 = ?, MOBILE1 = ? , DESKTOP2 = ?, MOBILE2 = ?  WHERE ID =1';
            let media_img_data = [imgPathJson.desktop1, imgPathJson.mobile1,imgPathJson.desktop2, imgPathJson.mobile2];

            const stmtImg = db.prepare(sql_media);
            const update_res = stmtImg.run(media_img_data);
            jsonMsg.msg = "更新成功";
            jsonMsg.code = 200;
            res.send(jsonMsg);

        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試-" + err.message,
            code: 500,
        }

        res.send(jsonMsg);
    }
});


/* Admin user - 更新產品類型排序 */
app.post('/update_product_type_order', function (req, res) {
    try {
        extendSession(req)
        let jsonMsg = {};
        let type = req.body.type;
        let order = req.body.order;
        let data = [order,type];
        let sql = 'UPDATE PRODUCT SET TYPE_ORDER = ? WHERE TYPE =?';
        if (req.session.userLevel != 0) {
            let jsonMsg = {
                msg: "您沒有權限",
                code: 500,
            }
            res.send(jsonMsg);
            return;
        }
        if (checkLogin(req) && req.session.userLevel == 0) {
            const stmt = db.prepare(sql);
            const info = stmt.run(data);
            if (info.changes > 0) {
                jsonMsg.msg = "更新成功";
                jsonMsg.code = 200;
                res.send(jsonMsg);
            } else {
                jsonMsg.msg = "更新失敗";
                jsonMsg.code = 222;
                res.send(jsonMsg);
            }

        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試-" + err.message,
            code: 500,
        }

        res.send(jsonMsg);
    }
});

/* Admin user - 更新產品排序 */
app.post('/update_product_order', function (req, res) {
    try {
        extendSession(req)
        let jsonMsg = {};
        let product_id = req.body.id;
        let product_order = req.body.order;
        let data = [product_order,product_id];
        let sql = 'UPDATE PRODUCT SET PRODUCT_ORDER = ? WHERE ID =?';
        if (req.session.userLevel != 0) {
            let jsonMsg = {
                msg: "您沒有權限",
                code: 500,
            }
            res.send(jsonMsg);
            return;
        }
        if (checkLogin(req) && req.session.userLevel == 0) {
            const stmt = db.prepare(sql);
            const info = stmt.run(data);
            if (info.changes == 1) {
                jsonMsg.msg = "更新成功";
                jsonMsg.code = 200;
                res.send(jsonMsg);
            } else {
                jsonMsg.msg = "更新失敗";
                jsonMsg.code = 222;
                res.send(jsonMsg);
            }

        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試-" + err.message,
            code: 500,
        }

        res.send(jsonMsg);
    }
});

/* Admin user - 更新目錄 */
app.post('/update_recipe', function (req, res) {
    try {
        extendSession(req)
        let jsonMsg = {};
        let recipe = req.body.recipe;
        let data = [recipe.name, recipe.description, recipe.link, recipe.id];
        let sql = 'UPDATE RECIPE SET NAME = ?, DESCRIPTION = ?, LINK=? WHERE ID =?';
        if (req.session.userLevel != 0) {
            let jsonMsg = {
                msg: "您沒有權限",
                code: 500,
            }
            res.send(jsonMsg);
            return;
        }
        if (checkLogin(req) && req.session.userLevel == 0) {

            const stmt = db.prepare(sql);
            const info = stmt.run(data);
            if (info.changes == 1) {
                jsonMsg.msg = "更新成功";
                let img64 = recipe.newImg;
                if (img64) {
                    var base64Data = "";
                    var extension = base64MimeType(img64); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img64.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img64.replace("data:image/png;base64,", "");
                    jsonMsg.msg = "更新成功";

                    fs.writeFile("assets/" + recipe.img, base64Data, { encoding: 'base64' }, function (err) {
                        if (err != null)
                            jsonMsg.msg = "上傳圖片有問題";
                    });
                }
                jsonMsg.code = 200;
                res.send(jsonMsg);
            } else {
                jsonMsg.msg = "更新失敗";
                jsonMsg.code = 222;
                res.send(jsonMsg);
            }

        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試-" + err.message,
            code: 500,
        }

        res.send(jsonMsg);
    }
});

/* Admin user - 更新食譜 */
app.post('/update_recipe', function (req, res) {
    try {
        extendSession(req)
        let jsonMsg = {};
        let recipe = req.body.recipe;
        let data = [recipe.name, recipe.description, recipe.link, recipe.id];
        let sql = 'UPDATE RECIPE SET NAME = ?, DESCRIPTION = ?, LINK=? WHERE ID =?';
        if (req.session.userLevel != 0) {
            let jsonMsg = {
                msg: "您沒有權限",
                code: 500,
            }
            res.send(jsonMsg);
            return;
        }
        if (checkLogin(req) && req.session.userLevel == 0) {

            const stmt = db.prepare(sql);
            const info = stmt.run(data);
            if (info.changes == 1) {
                jsonMsg.msg = "更新成功";
                let img64 = recipe.newImg;
                if (img64) {
                    var base64Data = "";
                    var extension = base64MimeType(img64); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img64.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img64.replace("data:image/png;base64,", "");
                    jsonMsg.msg = "更新成功";

                    fs.writeFile("assets/" + recipe.img, base64Data, { encoding: 'base64' }, function (err) {
                        if (err != null)
                            jsonMsg.msg = "上傳圖片有問題";
                    });
                }
                jsonMsg.code = 200;
                res.send(jsonMsg);
            } else {
                jsonMsg.msg = "更新失敗";
                jsonMsg.code = 222;
                res.send(jsonMsg);
            }

        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試-" + err.message,
            code: 500,
        }

        res.send(jsonMsg);
    }
});



/* Admin user - 更新產品 */
app.post('/update_product', function (req, res) {
    try {
        extendSession(req)
        let jsonMsg = {};
        let product = req.body.product;
        let data = [product.name, product.type, product.price, product.d_price, product.description, product.cook, product.unit, product.delivery, product.id];
        let sql = 'UPDATE PRODUCT SET NAME = ?, TYPE = ? , PRICE = ?, D_PRICE = ?, DESCRIPTION = ?, COOK = ?, UNIT = ?, DELIVERY = ? WHERE ID =?';
        if (req.session.userLevel != 0) {
            let jsonMsg = {
                msg: "您沒有權限",
                code: 500,
            }
            res.send(jsonMsg);
            return;
        }
        if (checkLogin(req) && req.session.userLevel == 0) {

            const stmt = db.prepare(sql);
            const info = stmt.run(data);
            if (info.changes == 1) {
                jsonMsg.msg = "更新成功";
                let img64 = product.newImg;
                if (img64) {
                    var base64Data = "";
                    var extension = base64MimeType(img64); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img64.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img64.replace("data:image/png;base64,", "");
                    jsonMsg.msg = "更新成功";

                    fs.writeFile("assets/" + product.img, base64Data, { encoding: 'base64' }, function (err) {
                        if (err != null)
                            jsonMsg.msg = "上傳圖片有問題";
                    });
                }
                jsonMsg.code = 200;
                res.send(jsonMsg);
            } else {
                jsonMsg.msg = "更新失敗";
                jsonMsg.code = 222;
                res.send(jsonMsg);
            }

        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試-" + err.message,
            code: 500,
        }

        res.send(jsonMsg);
    }
});


/* Admin user - 更新冷凍產品 */
app.post('/update_frozen_product', function (req, res) {
    try {
        extendSession(req)
        let jsonMsg = {};
        let id = req.body.id;
        let val = req.body.val == true ? 1 : 0;
        if (req.session.userLevel != 0) {
            let jsonMsg = {
                msg: "您沒有權限",
                code: 500,
            }
            res.send(jsonMsg);
            return;
        }
        if (checkLogin(req) && req.session.userLevel == 0) {
            let data = [val, id];
            let sql = 'UPDATE PRODUCT SET FROZEN = ? WHERE ID =?';
            const stmt = db.prepare(sql);
            const info = stmt.run(data);
            if (info.changes >= 1) {
                jsonMsg.msg = "更新成功";
                jsonMsg.code = 200;
                res.send(jsonMsg);
            } else {
                jsonMsg.msg = "更新失敗";
                jsonMsg.code = 222;
                res.send(jsonMsg);
            }

        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試-" + err.message,
            code: 500,
        }

        res.send(jsonMsg);
    }
});

app.post('/add_media', function (req, res) {
    try {
        extendSession(req)
        let jsonMsg = {};
        let media = req.body.media;
        let data = [media.title, media.subtitle, media.description, media.show2];
        let sql = 'INSERT INTO MEDIA (TITLE, SUBTITLE, DESCRIPTION, SHOW2) VALUES (?,?,?,?)';
        if (req.session.userLevel != 0) {
            let jsonMsg = {
                msg: "您沒有權限",
                code: 500,
            }
            res.send(jsonMsg);
            return;
        }
        if (checkLogin(req) && req.session.userLevel == 0) {
            const stmt = db.prepare(sql);
            const info = stmt.run(data);
            var mediaId = info.lastInsertRowid;
            if (info.changes == 1) {
                jsonMsg.msg = "更新成功";
                let imgPathJson = {
                    img1: media.img1,
                    img2: media.img2,
                    img3: media.img3,
                    img4: media.img4,
                    img5: media.img5,
                    img6: media.img6,
                    img7: media.img7,
                    img8: media.img8,
                    img9: media.img9,
                    img10: media.img10,
                    img11: media.img11,
                    img12: media.img12,
                    img13: media.img13,
                    img14: media.img14,
                    img15: media.img15
                }
                /* pic 1 */
                let img64 = media.newImg;
                if (img64) {
                    var extension = base64MimeType(img64); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img64.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img64.replace("data:image/png;base64,", "");
                    jsonMsg.msg = "更新成功";
                    if (media.img1) {
                        fs.writeFile("assets/" + media.img1, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {

                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                        imgPathJson.img1 = imgPath;
                    }
                }
                /* pic 2 */
                let img642 = media.newImg2;
                if (img642) {
                    var extension = base64MimeType(img642); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img642.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img642.replace("data:image/png;base64,", "");
                    jsonMsg.msg = "更新成功";
                    if (media.img2) {
                        fs.writeFile("assets/" + media.img2, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {
                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";

                        });
                        imgPathJson.img2 = imgPath;
                    }
                }
                /* pic 3 */
                let img643 = media.newImg3;
                if (img643) {
                    var extension = base64MimeType(img643); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img643.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img643.replace("data:image/png;base64,", "");
                    jsonMsg.msg = "更新成功";
                    if (media.img3) {
                        fs.writeFile("assets/" + media.img3, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {
                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";

                        });
                        imgPathJson.img3 = imgPath;
                    }
                }
                /* pic 4 */
                let img644 = media.newImg4;
                if (img644) {
                    var extension = base64MimeType(img644); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img644.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img644.replace("data:image/png;base64,", "");
                    jsonMsg.msg = "更新成功";
                    if (media.img4) {
                        fs.writeFile("assets/" + media.img4, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {
                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";

                        });
                        imgPathJson.img4 = imgPath;
                    }
                }
                /* pic 5 */
                let img645 = media.newImg5;
                if (img645) {
                    var extension = base64MimeType(img645); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img645.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img645.replace("data:image/png;base64,", "");
                    jsonMsg.msg = "更新成功";
                    if (media.img5) {
                        fs.writeFile("assets/" + media.img5, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {
                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";

                        });
                        imgPathJson.img5 = imgPath;
                    }
                }
                /* pic 6 */
                let img646 = media.newImg6;
                if (img646) {
                    var extension = base64MimeType(img646); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img646.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img646.replace("data:image/png;base64,", "");
                    jsonMsg.msg = "更新成功";
                    if (media.img6) {
                        fs.writeFile("assets/" + media.img6, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {
                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";

                        });
                        imgPathJson.img6 = imgPath;
                    }
                }
                /* pic 7 */
                let img647 = media.newImg7;
                if (img647) {
                    var extension = base64MimeType(img647); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img647.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img647.replace("data:image/png;base64,", "");
                    jsonMsg.msg = "更新成功";
                    if (media.img7) {
                        fs.writeFile("assets/" + media.img7, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {
                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";

                        });
                        imgPathJson.img7 = imgPath;
                    }
                }
                /* pic 8 */
                let img648 = media.newImg8;
                if (img648) {
                    var extension = base64MimeType(img648); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img648.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img648.replace("data:image/png;base64,", "");
                    jsonMsg.msg = "更新成功";
                    if (media.img8) {
                        fs.writeFile("assets/" + media.img8, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {
                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";

                        });
                        imgPathJson.img8 = imgPath;
                    }
                }
                /* pic 9 */
                let img649 = media.newImg9;
                if (img649) {
                    var extension = base64MimeType(img649); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img649.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img649.replace("data:image/png;base64,", "");
                    jsonMsg.msg = "更新成功";
                    if (media.img9) {
                        fs.writeFile("assets/" + media.img9, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {
                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";

                        });
                        imgPathJson.img9 = imgPath;
                    }
                }
                /* pic 10 */
                let img6410 = media.newImg10;
                if (img6410) {
                    var extension = base64MimeType(img6410); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img6410.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img6410.replace("data:image/png;base64,", "");

                    jsonMsg.msg = "更新成功";
                    if (media.img10) {
                        fs.writeFile("assets/" + media.img10, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {
                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";

                        });
                        imgPathJson.img10 = imgPath;
                    }
                }
                /* pic 11 */
                let img6411 = media.newImg11;
                if (img6411) {
                    var extension = base64MimeType(img6411); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img6411.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img6411.replace("data:image/png;base64,", "");

                    jsonMsg.msg = "更新成功";
                    if (media.img11) {
                        fs.writeFile("assets/" + media.img11, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {
                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";

                        });
                        imgPathJson.img11 = imgPath;
                    }
                }
                /* pic 12 */
                let img6412 = media.newImg12;
                if (img6412) {
                    var extension = base64MimeType(img6412); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img6412.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img6412.replace("data:image/png;base64,", "");

                    jsonMsg.msg = "更新成功";
                    if (media.img12) {
                        fs.writeFile("assets/" + media.img12, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {
                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";

                        });
                        imgPathJson.img12 = imgPath;
                    }
                }
                /* pic 13 */
                let img6413 = media.newImg13;
                if (img6413) {
                    var extension = base64MimeType(img6413); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img6413.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img6413.replace("data:image/png;base64,", "");
                    jsonMsg.msg = "更新成功";
                    if (media.img13) {
                        fs.writeFile("assets/" + media.img13, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {
                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";

                        });
                        imgPathJson.img13 = imgPath;
                    }
                }
                /* pic 14 */
                let img6414 = media.newImg14;
                if (img6414) {
                    var extension = base64MimeType(img6414); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img6414.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img6414.replace("data:image/png;base64,", "");
                    jsonMsg.msg = "更新成功";
                    if (media.img14) {
                        fs.writeFile("assets/" + media.img14, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {
                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";

                        });
                        imgPathJson.img14 = imgPath;
                    }
                }
                /* pic 15 */
                let img6415 = media.newImg15;
                if (img6415) {
                    var extension = base64MimeType(img6415); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img6415.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img6415.replace("data:image/png;base64,", "");
                    jsonMsg.msg = "更新成功";
                    if (media.img15) {
                        fs.writeFile("assets/" + media.img15, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";
                        });
                    } else {
                        let imgPath = "images/media/" + _uuid() + "." + extension;
                        fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                            if (err != null)
                                jsonMsg.msg = "上傳圖片有問題";

                        });
                        imgPathJson.img15 = imgPath;
                    }
                }
                let sql_media = 'UPDATE MEDIA SET IMG1 = ?, IMG2 = ? , IMG3 = ? , IMG4 = ? , IMG5 = ? ,IMG6 = ?, IMG7 = ? , IMG8 = ? , IMG9 = ? , IMG10 = ?, IMG11 = ?, IMG12 = ? , IMG13 = ? , IMG14 = ? , IMG15 = ?  WHERE ID =?';
                let media_img_data = [imgPathJson.img1, imgPathJson.img2, imgPathJson.img3, imgPathJson.img4, imgPathJson.img5, imgPathJson.img6, imgPathJson.img7, imgPathJson.img8, imgPathJson.img9, imgPathJson.img10, imgPathJson.img11, imgPathJson.img12, imgPathJson.img13, imgPathJson.img14, imgPathJson.img15, mediaId];

                const stmtImg = db.prepare(sql_media);
                const update_res = stmtImg.run(media_img_data);
                jsonMsg.code = 200;
                res.send(jsonMsg);
            } else {
                jsonMsg.msg = "更新失敗";
                jsonMsg.code = 222;
                res.send(jsonMsg);
            }

        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試-" + err.message,
            code: 500,
        }

        res.send(jsonMsg);
    }
});

/* Admin user - 新增產品 */

app.post('/add_product', function (req, res) {
    try {
        let jsonMsg = {};
        let product = req.body.product;
        let imgPath = "images/product/" + _uuid() + ".jpg";
        let data = [product.name, product.type, product.price, product.d_price, product.description, product.cook, product.unit, product.delivery, imgPath]
        let sql = 'INSERT INTO PRODUCT (NAME, TYPE, PRICE, D_PRICE, DESCRIPTION, COOK, UNIT, DELIVERY,IMG) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ? )';
        if (req.session.userLevel != 0) {
            let jsonMsg = {
                msg: "您沒有權限",
                code: 500,
            }
            res.send(jsonMsg);
            return;
        }
        if (checkLogin(req) && req.session.userLevel == 0) {
            const stmt = db.prepare(sql);
            const info = stmt.run(data);
            if (info.changes == 1) {
                jsonMsg.msg = "更新成功";
                let img64 = product.newImg;
                if (img64) {
                    var base64Data = "";
                    var extension = base64MimeType(img64); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img64.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img64.replace("data:image/png;base64,", "");
                    jsonMsg.msg = "更新成功";

                    fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                        if (err != null)
                            jsonMsg.msg = "上傳圖片有問題";
                    });

                }

                jsonMsg.code = 200;
                res.send(jsonMsg);
            } else {
                jsonMsg.msg = "更新失敗";
                jsonMsg.code = 222;
                res.send(jsonMsg);
            }
        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試-" + err.message,
            code: 500,
        }

        res.send(jsonMsg);
    }
});


/* Admin user - 新增食譜 */

app.post('/add_recipe', function (req, res) {
    try {
        let jsonMsg = {};
        let recipe = req.body.recipe;
        let imgPath = "images/recipe/" + _uuid() + ".jpg";
        let data = [recipe.name, recipe.description, recipe.link, imgPath]
        let sql = 'INSERT INTO RECIPE (NAME, DESCRIPTION, LINK,IMG) VALUES (?, ?, ?, ?)';
        if (req.session.userLevel != 0) {
            let jsonMsg = {
                msg: "您沒有權限",
                code: 500,
            }
            res.send(jsonMsg);
            return;
        }
        if (checkLogin(req) && req.session.userLevel == 0) {
            const stmt = db.prepare(sql);
            const info = stmt.run(data);
            if (info.changes == 1) {
                jsonMsg.msg = "更新成功";
                let img64 = recipe.newImg;
                if (img64) {
                    var base64Data = "";
                    var extension = base64MimeType(img64); // "image/png"
                    if (extension == 'jpeg' || extension == 'jpg') {
                        base64Data = img64.replace("data:image/jpeg;base64,", "");
                        extension = "jpg"
                    }
                    else
                        base64Data = img64.replace("data:image/png;base64,", "");
                    jsonMsg.msg = "更新成功";

                    fs.writeFile("assets/" + imgPath, base64Data, { encoding: 'base64' }, function (err) {
                        if (err != null)
                            jsonMsg.msg = "上傳圖片有問題";
                    });

                }

                jsonMsg.code = 200;
                res.send(jsonMsg);
            } else {
                jsonMsg.msg = "更新失敗";
                jsonMsg.code = 222;
                res.send(jsonMsg);
            }
        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試-" + err.message,
            code: 500,
        }

        res.send(jsonMsg);
    }
});


/* Admin user - 新增消息 */

app.post('/add_news', function (req, res) {
    try {
        let jsonMsg = {};
        let news = req.body.news;
        let show = news.show == true ? 1 : 0;
        let data = [news.title, news.message, news.date, news.link, show]
        let sql = 'INSERT INTO NEWS (TITLE, MESSAGE, DATE,LINK, SHOW) VALUES (?, ?, ?, ?,? )';
        if (req.session.userLevel != 0) {
            let jsonMsg = {
                msg: "您沒有權限",
                code: 500,
            }
            res.send(jsonMsg);
            return;
        }
        if (checkLogin(req) && req.session.userLevel == 0) {
            const stmt = db.prepare(sql);
            const info = stmt.run(data);
            if (info.changes == 1) {
                jsonMsg.msg = "更新成功";
                jsonMsg.code = 200;
                res.send(jsonMsg);
            } else {
                jsonMsg.msg = "更新失敗";
                jsonMsg.code = 222;
                res.send(jsonMsg);
            }
        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試-" + err.message,
            code: 500,
        }

        res.send(jsonMsg);
    }
});


/*Admin User - 更新規則*/
app.post('/update_notice', function (req, res) {
    try {

        let jsonMsg = {};
        let rule = req.body.rule;
        let data = [rule.notice, rule.shipping_fee, rule.frozen_shipping_fee, rule.free_shipping_price, rule.id];
        let sql = 'UPDATE RULES SET NOTICE = ?,SHIPPING_FEE = ?, FROZEN_SHIPPING_FEE = ?, FREE_SHIPPING_PRICE = ?  WHERE ID =?';
        if (req.session.userLevel != 0) {
            let jsonMsg = {
                msg: "您沒有權限",
                code: 500,
            }
            res.send(jsonMsg);
            return;
        }

        if (checkLogin(req) && req.session.userLevel == 0) {
            const stmt = db.prepare(sql);
            const info = stmt.run(data);
            if (info.changes >= 1) {
                jsonMsg.msg = "更新成功";
                jsonMsg.code = 200;
                res.send(jsonMsg);
            } else {
                jsonMsg.msg = "更新失敗";
                jsonMsg.code = 222;
                res.send(jsonMsg);
            }
        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試-" + err,
            code: 500,
        }
        res.send(jsonMsg);
    }
});

/*Admin User - 更新店家資訊*/
app.post('/update_info', function (req, res) {
    try {

        let userId = req.session.userId;
        let jsonMsg = {};
        let info = req.body.info;
        let data = [info.address, info.phone, info.tax, info.email];
        if (req.session.userLevel != 0) {
            let jsonMsg = {
                msg: "您沒有權限",
                code: 500,
            }
            res.send(jsonMsg);
            return;
        }
        if (checkLogin(req) && req.session.userLevel == 0) {
            const stmt = db.prepare('UPDATE INFO SET ADDRESS = ? , PHONE = ?, TAX = ?, EMAIL = ? WHERE ID = 1');
            const info = stmt.run(data);
            if (info.changes == 1) {
                jsonMsg.code = 200;
                jsonMsg.msg = "更新成功";
                res.send(jsonMsg);
            } else {
                jsonMsg.code = 222;
                jsonMsg.msg = "更新失敗";
                res.send(jsonMsg);
            }
        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試 -" + err.message,
            code: 500,
        }

        res.send(jsonMsg);
    }
});

/* 使用者更新資料*/
app.post('/update_user', function (req, res) {
    try {
        let userId = req.session.userId;
        let jsonMsg = {};
        let userData = req.body.userData;
        let data = [];
        let sql = 'UPDATE USER SET NAME = ? , PHONE = ?, BIRTHDAY = ? WHERE ID = ?';
        if (userData.newPassword)
            sql = 'UPDATE USER SET NAME = ? , PHONE = ?, BIRTHDAY = ? , PASSWORD = ? WHERE ID = ?';
        if (checkLogin(req) && userId) {
            data = [userData.name, userData.phone, userData.birthday, userData.id];
            if (userData.newPassword) {
                var salt = bcrypt.genSaltSync(10);
                var hash = bcrypt.hashSync(userData.newPassword, salt);
                data = [userData.name, userData.phone, userData.birthday, hash, userData.id];

            }
            const stmt = db.prepare(sql);
            const info = stmt.run(data);
            if (info.changes == 1) {
                jsonMsg.code = 200;
                jsonMsg.msg = "更新成功";
                res.send(jsonMsg);
            } else {
                jsonMsg.code = 222;
                jsonMsg.msg = "更新失敗";
                res.send(jsonMsg);
            }
        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試 -" + err.message,
            code: 500,
        }

        res.send(jsonMsg);
    }
});



/*User - 再次付款*/
app.post('/repay', function (req, res) {
    try {
        let jsonMsg = {};
        let product_data = req.body.data;

        if (checkLogin(req)) {
            let new_order_id = _uuid();
            let data = [new_order_id, product_data.order_id];
            const stmt = db.prepare('UPDATE PRODUCT_ORDER_LIST SET ORDER_ID = ? WHERE ORDER_ID = ?');
            const info = stmt.run(data);
            if (info.changes >= 1) {
                jsonMsg.code = 200;
                jsonMsg.msg = "更新成功";
                const stmt2 = db.prepare('UPDATE PRODUCT_ORDER SET ORDER_ID = ? WHERE ORDER_ID = ?');
                const info2 = stmt2.run(data);
                if (info2.changes >= 1) {
                    jsonMsg.new_order_id = new_order_id;
                    res.send(jsonMsg);
                } else {
                    jsonMsg.code = 222;
                    jsonMsg.msg = "更新失敗";
                    res.send(jsonMsg);
                }
            } else {
                jsonMsg.code = 222;
                jsonMsg.msg = "更新失敗";
                res.send(jsonMsg);
            }
        } else {
            let jsonMsg = {
                msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
                code: 401,
            }
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試 -" + err.message,
            code: 500,
        }

        res.send(jsonMsg);
    }
});


/**************************Login Not Required *************************/

/* getting the product information */
app.get('/product_menu', function (req, res) {
    try {
        extendSession(req)
        const rows = db.prepare("SELECT * FROM PRODUCT_MENU").all();
        res.send(rows);
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試 - " + err.message,
            code: 500,
        }
        res.send(jsonMsg);
    }
});

/* getting the product information */
app.get('/product', function (req, res) {
    try {
        extendSession(req)
        const rows = db.prepare("SELECT * FROM PRODUCT ORDER BY TYPE_ORDER").all();
        res.send(rows);
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試 - " + err.message,
            code: 500,
        }

        res.send(jsonMsg);
    }
});

/* getting the product information */
app.get('/product_type', function (req, res) {
    try {
        extendSession(req)
        const rows = db.prepare("SELECT DISTINCT TYPE,TYPE_ORDER FROM PRODUCT ORDER BY TYPE_ORDER").all();
        res.send(rows);
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試 - " + err.message,
            code: 500,
        }

        res.send(jsonMsg);
    }
});



/* getting the media information */
app.get('/media', function (req, res) {
    try {
        extendSession(req)
        const rows = db.prepare("SELECT * FROM MEDIA ORDER BY ID DESC").all();
        res.send(rows);
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試",
            code: 500,
        }
        res.send(jsonMsg);
    }

});


/* getting the recipe information */
app.get('/recipe', function (req, res) {
    try {
        extendSession(req)
        const rows = db.prepare("SELECT * FROM RECIPE").all();
        res.send(rows);
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試",
            code: 500,
        }
        res.send(jsonMsg);
    }

});


/* getting 最新消息*/
app.get('/news', function (req, res) {
    try {
        extendSession(req)
        const rows = db.prepare("SELECT * FROM NEWS").all();
        res.send(rows);
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試" + err,
            code: 500,
        }
        res.send(jsonMsg);
    }

});

/* getting social*/
app.get('/social', function (req, res) {
    try {
        extendSession(req)
        const rows = db.prepare("SELECT * FROM SOCIAL").all();
        res.send(rows);
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試" + err,
            code: 500,
        }
        res.send(jsonMsg);
    }

});


/* getting banner*/
app.get('/banner', function (req, res) {
    try {
        extendSession(req)
        const rows = db.prepare("SELECT * FROM BANNER").all();
        res.send(rows);
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試" + err,
            code: 500,
        }
        res.send(jsonMsg);
    }

});



/* user saving the placed order  */
app.post('/save_order', function (req, res) {
    if (checkLogin(req)) {
        let order = req.body.order;
        let userId = req.session.userId;
        let city = req.body.city;
        let address = req.body.address;
        let postcode = req.body.postcode;
        let price = req.body.price;
        let jsonMsg = {
            msg: "請確實填好資料",
            code: 412
        }
        if (order == '' || order == null) {
            res.send(jsonMsg);
        } else if (userId == '' || userId == null || userId == -1) {
            res.send(jsonMsg);
        } else {
            try {
                //double check the price from db
                let productMap = new Map();
                const productRows = db.prepare("SELECT * FROM PRODUCT").all();
                for (let i = 0; i < productRows.length; i++) {
                    let p = productRows[i].d_price;
                    if (productRows[i].d_price == "" || productRows[i].d_price == null) {
                        p = productRows[i].price;
                    }
                    productMap.set(productRows[i].id, p);
                }
                let confirmPrice = 0;
                //get product names
                let orderId = _uuid();
                let totalPrice = 0;
                let inserSql = "INSERT INTO PRODUCT_ORDER (PRODUCT_ID,PRODUCT_AMOUT,PRODUCT_PRICE,USER_ID,ORDER_ID) VALUES ";
                for (var i = 0; i < order.length - 1; i++) {
                    let product = order[i];
                    inserSql += "(" + product.id + "," + product.amount + "," + product.price + "," + userId + ",'" + orderId + "'),";
                    totalPrice += (product.amount * product.price);
                    confirmPrice += productMap.get(product.id) * product.amount;
                }
                let lastItme = order[order.length - 1];
                inserSql += "(" + lastItme.id + "," + lastItme.amount + "," + lastItme.price + "," + userId + ",'" + orderId + "');";
                totalPrice += (lastItme.amount * lastItme.price);
                confirmPrice += productMap.get(lastItme.id) * lastItme.amount;

                if (totalPrice == confirmPrice) {
                    // if (totalPrice < 3000) {
                    //     totalPrice += 150;
                    // }
                    //save order for user
                    const stmt = db.prepare(inserSql);
                    const info = stmt.run();
                    if (info.changes > 0) {
                        //save order for admin with address
                        let insertOrderData = [orderId, city, address, postcode + '', userId, price + ''];
                        let inserOrderSql = "INSERT INTO PRODUCT_ORDER_LIST (ORDER_ID,CITY,ADDRESS,POSTCODE,USER_ID,PRICE) VALUES(?,?,?,?,?,?)";
                        const stmtOrder = db.prepare(inserOrderSql);
                        const infoOrder = stmtOrder.run(insertOrderData);
                        if (infoOrder.changes > 0) {
                            sendEmail(req.session.userEmail,"上海火腿-訂單建立成功","您的訂單號碼為："+orderId+"<br> 可以在個人頁面查看訂單內容<br>注意：請完成付款後貨品才會寄出")
                            const orderRows = db.prepare('SELECT PRODUCT_ORDER.id , PRODUCT_ORDER.product_id , PRODUCT_ORDER.user_id ,PRODUCT_ORDER.product_price ,  PRODUCT_ORDER.product_amout , PRODUCT_ORDER.order_id ,PRODUCT_ORDER.date ,PRODUCT.name FROM PRODUCT_ORDER LEFT JOIN PRODUCT WHERE PRODUCT_ORDER.product_id=PRODUCT.id AND PRODUCT_ORDER.order_id=?').all(orderId);
                            let orderMessage = "訂單內容為：<br>";
                            for (var i = 0; i < orderRows.length; i++) {
                                let p = orderRows[i].product_amout * orderRows[i].product_price;
                                orderMessage += orderRows[i].name + " x " + orderRows[i].product_amout + "=" + p + "<br>";
                            }
                            orderMessage = orderMessage + "<hr><br>總共(加運費) $" + price;
                            sendEmail("scweitinglo@gmail.com","上海火腿-新訂單","新的訂單號碼為："+orderId+"<br><br>"+orderMessage+"<br><a href='http://www.sanhiham.com.tw/#!/admin-order?id="+orderId+"'>查看訂單</a>");
                            sendEmail("sanhiham@yahoo.com.tw","上海火腿-新訂單","新的訂單號碼為："+orderId+"<br><br>"+orderMessage+"<br><a href='http://www.sanhiham.com.tw/#!/admin-order?id="+orderId+"'>查看訂單</a>");

                            jsonMsg.msg = "成功下單！";
                            jsonMsg.code = 200;
                            jsonMsg.orderId = orderId;
                            let userData = [city, postcode, address, req.session.userEmail];
                            const stmt2 = db.prepare('UPDATE USER SET CITY = ?, POSTCODE=?, ADDRESS=? WHERE EMAIL = ?');
                            const info2 = stmt2.run(userData);
                            res.clearCookie('product');
                            res.send(jsonMsg);
                        }
                    } else {
                        jsonMsg.msg = "更新失敗";
                        jsonMsg.code = 222;
                        res.send(jsonMsg);
                    }
                } else {
                    jsonMsg = {
                        msg: "系統出現問題，請稍候再試 - 價錢出現誤差",
                        code: 205
                    }
                    res.send(jsonMsg);
                }
                //add shipping fee
            } catch (err) {
                jsonMsg = {
                    msg: "系統出現問題，請稍候再試 -" + err.message,
                    code: 412
                }
                res.send(jsonMsg);
            }
        }
    } else {
        let jsonMsg = {
            msg: "由於閒置時間太長，您已經被登出，請重新登入一次",
            code: 401,
        }
        res.send(jsonMsg);
    }

});

/* Registering a new user */
app.post('/register_user', function (req, res) {
    let name = (typeof req.body.name !== 'undefined') && req.body.name != '' ? req.body.name : null;
    let email = (typeof req.body.email !== 'undefined') && req.body.email != '' ? req.body.email : null;
    let birthday = (typeof req.body.birthday !== 'undefined') && req.body.birthday != '' ? req.body.birthday : null;
    let phone = (typeof req.body.phone !== 'undefined') && req.body.phone != '' ? req.body.phone : '';
    let password = (typeof req.body.password !== 'undefined') && req.body.password != '' ? req.body.password : null;
    let jsonMsg = {
        msg: "請確實填好資料",
        code: 412,
    }
    if (name == '' || name == null || email == '' || email == null || password == '' || password == null || birthday == '' || birthday == null) {
        res.send(jsonMsg);
    } else {
        try {
            var salt = bcrypt.genSaltSync(10);
            var hash = bcrypt.hashSync(password, salt);
            const stmt = db.prepare('INSERT INTO USER (NAME,EMAIL,PHONE,BIRTHDAY,PASSWORD) VALUES(?,?,?,?,?)');
            let data = [name, email, phone + '', birthday, hash];
            let dbInfo = stmt.run(data);
            if (dbInfo.changes == 1) {
                sendEmail(email, "上海火腿-歡迎您的加入", "您可以開始再上海火腿官網下單囉")
                jsonMsg.code = 200;
                jsonMsg.msg = "使用者已成功加入";
                res.send(jsonMsg);
            }
        } catch (err) {
            jsonMsg = {
                msg: "密碼輸入有問題，請重新輸入一次",
                code: 222,
            }
            if (err.message == 'UNIQUE constraint failed: USER.email')
                jsonMsg.msg = "這個電子信箱已經被註冊過了，請試試看別的";
            res.send(jsonMsg);
        }
    }
});

/* Logging into the registered account */
app.post('/signin', function (req, res) {
    let email = (typeof req.body.email !== 'undefined') && req.body.email != '' ? req.body.email : null;
    let password = (typeof req.body.password !== 'undefined') && req.body.password != '' ? req.body.password : null;
    let jsonMsg = {
        msg: "請確實填好資料",
        code: 412,
    }
    if (email == '' || email == null || password == '' || password == null) {
        res.send(jsonMsg);
    } else {
        let jsonMsg = {
            msg: "系統有問題，請稍候再試",
            code: 500
        }
        try {
            const rows = db.prepare("SELECT * FROM USER WHERE EMAIL=?").get(email);
            if (rows) {
                if (bcrypt.compareSync(password, rows.password)) {
                    jsonMsg.msg = "登入成功";
                    jsonMsg.code = 200;
                    req.session.userEmail = rows.email;
                    req.session.userId = rows.id;
                    req.session.userName = rows.name;
                    req.session.userLevel = rows.level;
                    req.session.loggedin = true;
                    res.send(jsonMsg);
                } else {
                    jsonMsg.msg = "帳號或密碼輸入不正確"
                    res.send(jsonMsg);
                }
            } else {
                jsonMsg.msg = "登入失敗";
                res.send(jsonMsg);
            }
        } catch (err) {
            res.send(jsonMsg);
        }
    }
});

/* Loggout the user account */
app.post('/signout', function (req, res) {
    let jsonMsg = {
        msg: "登出成功",
        code: 200,
    }
    try {
        res.clearCookie('session-id');
        req.session.destroy();
        res.send(jsonMsg);

    } catch (err) {
        jsonMsg.msg = err.message;
        res.send(jsonMsg);
    }

});





/* 使用者流言 */
app.post('/send_comment', function (req, res) {
    try {
        extendSession(req)
        let jsonMsg = {};
        let comment = req.body.comment;
        const stmt = db.prepare('INSERT INTO COMMENT (NAME,EMAIL,PHONE,MESSAGE) VALUES(?,?,?,?)');
        let data = [comment.name, comment.email, comment.phone + '', comment.message];
        let dbInfo = stmt.run(data);
        if (dbInfo.changes == 1) {
            jsonMsg.msg = "留言成功";
            jsonMsg.code = 200;
            res.send(jsonMsg);
        } else {
            jsonMsg.msg = "留言失敗";
            jsonMsg.code = 222;
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試 - " + err.message,
            code: 500,
        }
        res.send(jsonMsg);
    }
});


/*顯示店家資訊*/
app.get('/info', function (req, res) {
    try {
        const rows = db.prepare('SELECT * FROM INFO').get();
        res.send(rows);
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試",
            code: 500,
        }

        res.send(jsonMsg);
    }
});

/* Payment status update from 綠界*/
app.post('/payment_res', function (req, res) {
    // console.log("*******payment_res********")
    // console.log(req.body.MerchantTradeNo)
    // console.log(req.body.PaymentDate)

    // console.log(req.body.TradeAmt)

    // console.log(req.body.PaymentTypeChargeFee)
    // console.log(req.body.RtnCode)
    // console.log("***************")

    let orderId = req.body.MerchantTradeNo;
    let payment_date = req.body.PaymentDate;
    let payment_amount = req.body.TradeAmt;
    let charge_fee = req.body.PaymentTypeChargeFee;
    let payment_code = req.body.RtnCode;

    if (payment_code == 1) {
        let data = [payment_code, payment_amount, payment_date, "card", orderId];
        const stmt = db.prepare('UPDATE PRODUCT_ORDER_LIST SET PAY_STATUS = ? , PAY_AMOUNT = ?,PAY_DATE = ?, PAY_TYPE=? WHERE ORDER_ID = ?');
        const info = stmt.run(data);
    }

});


/* Payment status update from 綠界*/
app.post('/payment_res_atm', function (req, res) {
    // console.log("*******payment_res_atm********")
    // console.log(req.body.MerchantTradeNo)
    // console.log(req.body.TradeDate)

    // console.log(req.body.TradeAmt)

    // console.log(req.body.BankCode)
    // console.log(req.body.vAccount)
    // console.log(req.body.ExpireDate)

    // console.log(req.body.RtnCode)
    // console.log("***************")

    let orderId = req.body.MerchantTradeNo;
    let payment_date = req.body.TradeDate;
    let payment_amount = req.body.TradeAmt;
    let payment_bank = req.body.BankCode;

    let payment_account = req.body.vAccount;
    let payment_expire = req.body.ExpireDate;

    let payment_code = req.body.RtnCode;

    if (payment_code == 2) {
        let data = [payment_code, payment_amount, payment_date, "ATM", payment_bank, payment_account, payment_expire, orderId];
        const stmt = db.prepare('UPDATE PRODUCT_ORDER_LIST SET PAY_STATUS = ? , PAY_AMOUNT = ?, PAY_DATE = ? , PAY_TYPE = ? , PAY_ATM_BANK = ? , PAY_ATM_ACCOUNT = ?, PAY_ATM_EXPIRE_DATE = ?  WHERE ORDER_ID = ?');
        const info = stmt.run(data);
    }
    res.send("1")

});

/* Payment status update from 綠界*/
app.post('/payment_res_atm_res', function (req, res) {
    // console.log("*******payment_res_atm_res********")
    // console.log(req.body.MerchantTradeNo)
    // console.log(req.body.PaymentDate)

    // console.log(req.body.TradeAmt)

    // console.log(req.body.PaymentTypeChargeFee)
    // console.log(req.body.RtnCode)
    // console.log("***************")

    let orderId = req.body.MerchantTradeNo;
    let payment_date = req.body.PaymentDate;
    let payment_amount = req.body.TradeAmt;
    let charge_fee = req.body.PaymentTypeChargeFee;
    let payment_code = req.body.RtnCode;

    if (payment_code == 1) {
        let data = [payment_code, payment_amount, payment_date, "ATM", orderId];
        const stmt = db.prepare('UPDATE PRODUCT_ORDER_LIST SET PAY_STATUS = ? , PAY_AMOUNT = ?,PAY_DATE = ?, PAY_TYPE=? WHERE ORDER_ID = ?');
        const info = stmt.run(data);
    }
});




app.post('/send_new_password', function (req, res) {
    try {
        let jsonMsg = {};
        let email = req.body.email;
        let tempPassword = _uuid();
        var salt = bcrypt.genSaltSync(10);
        var hash = bcrypt.hashSync(tempPassword, salt);
        let data = [hash, email];
        const stmt = db.prepare('UPDATE USER SET PASSWORD = ? WHERE EMAIL = ?');
        const info = stmt.run(data);
        if (info.changes > 0) {
            if (sendEmail(email, "上海火腿-忘記密碼", "您的新密碼為：" + tempPassword + "<br> 可以在個人頁面更新密碼")) {
                jsonMsg.code = 200;
                jsonMsg.msg = "更新成功";
                res.send(jsonMsg);
            }
            jsonMsg.code = 200;
            jsonMsg.msg = "新的密碼已寄出";
            res.send(jsonMsg);
        } else {
            jsonMsg.code = 222;
            jsonMsg.msg = "沒有找到輸入的會員電子信箱";
            res.send(jsonMsg);
        }
    } catch (err) {
        let jsonMsg = {
            msg: "系統出現問題，請稍候再試 -" + err.message,
            code: 500,
        }
        res.send(jsonMsg);
    }
});


app.get('/pay', function (req, res) {

    let date = NOW();
    let orderId = req.query.orderId;
    const rows = db.prepare('SELECT PRODUCT_ORDER.id , PRODUCT_ORDER.product_id , PRODUCT_ORDER.user_id ,PRODUCT_ORDER.product_price ,  PRODUCT_ORDER.product_amout , PRODUCT_ORDER.order_id ,PRODUCT_ORDER.date ,PRODUCT.name, PRODUCT.frozen FROM PRODUCT_ORDER LEFT JOIN PRODUCT WHERE PRODUCT_ORDER.product_id=PRODUCT.id AND PRODUCT_ORDER.order_id=?').all(orderId);
    let totalPrice = 0;
    let itemName = "";
    let hasFrozen = false;
    for (let i = 0; i < rows.length; i++) {
        itemName += rows[i].name + ": $" + rows[i].product_price + " x " + rows[i].product_amout + "#";
        totalPrice += parseInt(rows[i].product_price) * parseInt(rows[i].product_amout);
        if (rows[i].frozen == 1)
            hasFrozen = true;
    }
    const rule_rows = db.prepare('SELECT * FROM RULES').all();
    let basic_shipping_fee = 150;
    let frozen_shipping_fee = 200;
    let free_price = 3000;
    let shipping_fee = 0;
    for (let i = 0; i < rule_rows.length; i++) {
        basic_shipping_fee = rule_rows[i].shipping_fee;
        frozen_shipping_fee = rule_rows[i].frozen_shipping_fee;
        free_price = rule_rows[i].free_shipping_price;
    }
    if (totalPrice >= free_price) {
        shipping_fee = 0;
    } else if (hasFrozen) {
        shipping_fee = frozen_shipping_fee;
    } else {
        shipping_fee = basic_shipping_fee;
    }
    itemName += "運費: $" + shipping_fee;
    totalPrice += shipping_fee;
    let base_param = {
        MerchantTradeNo: orderId,
        MerchantTradeDate: date, //ex: 2017/02/13 15:45:30
        TotalAmount: "" + totalPrice,
        TradeDesc: '上海火腿購買',
        ItemName: itemName,
        ReturnURL: 'http://68.183.177.114/payment_res',
        // ChooseSubPayment: '',
        // OrderResultURL: 'http://68.183.177.114/#!/profile',
        // NeedExtraPaidInfo: '1',
        ClientBackURL: 'http://68.183.177.114/#!/profile',
        // ItemURL: 'http://item.test.tw',
        // Remark: '交易備註',
        // StoreID: '',
        // CustomField1: '',
        // CustomField2: '',
        // CustomField3: '',
        // CustomField4: ''
    };

    // 若要測試開立電子發票，請將inv_params內的"所有"參數取消註解 //
    let inv_params = {
        // RelateNumber: 'PLEASE MODIFY',  //請帶30碼uid ex: SJDFJGH24FJIL97G73653XM0VOMS4K
        // CustomerID: 'MEM_0000001',  //會員編號
        // CustomerIdentifier: '',   //統一編號
        // CustomerName: '測試買家',
        // CustomerAddr: '測試用地址',
        // CustomerPhone: '0123456789',
        // CustomerEmail: 'johndoe@test.com',
        // ClearanceMark: '2',
        // TaxType: '1',
        // CarruerType: '',
        // CarruerNum: '',
        // Donation: '2',
        // LoveCode: '',
        // Print: '1',
        // InvoiceItemName: '測試商品1|測試商品2',
        // InvoiceItemCount: '2|3',
        // InvoiceItemWord: '個|包',
        // InvoiceItemPrice: '35|10',
        // InvoiceItemTaxType: '1|1',
        // InvoiceRemark: '測試商品1的說明|測試商品2的說明',
        // DelayDay: '0',
        // InvType: '07'
    };

    let create = new ecpay_payment();
    let htm = create.payment_client.aio_check_out_credit_onetime(parameters = base_param, invoice = inv_params);
    res.send(htm);
})

app.get('/pay_atm', function (req, res) {

    let date = NOW();
    let orderId = req.query.orderId;
    const rows = db.prepare('SELECT PRODUCT_ORDER.id , PRODUCT_ORDER.product_id , PRODUCT_ORDER.user_id ,PRODUCT_ORDER.product_price ,  PRODUCT_ORDER.product_amout , PRODUCT_ORDER.order_id ,PRODUCT_ORDER.date ,PRODUCT.name, PRODUCT.frozen FROM PRODUCT_ORDER LEFT JOIN PRODUCT WHERE PRODUCT_ORDER.product_id=PRODUCT.id AND PRODUCT_ORDER.order_id=?').all(orderId);
    let totalPrice = 0;
    let itemName = "";
    let hasFrozen = false;
    for (let i = 0; i < rows.length; i++) {
        itemName += rows[i].name + ": $" + rows[i].product_price + " x " + rows[i].product_amout + "#";
        totalPrice += parseInt(rows[i].product_price) * parseInt(rows[i].product_amout);
        if (rows[i].frozen == 1)
            hasFrozen = true;
    }
    const rule_rows = db.prepare('SELECT * FROM RULES').all();
    let basic_shipping_fee = 150;
    let frozen_shipping_fee = 200;
    let free_price = 3000;
    let shipping_fee = 0;
    for (let i = 0; i < rule_rows.length; i++) {
        basic_shipping_fee = rule_rows[i].shipping_fee;
        frozen_shipping_fee = rule_rows[i].frozen_shipping_fee;
        free_price = rule_rows[i].free_shipping_price;
    }
    if (totalPrice >= free_price) {
        shipping_fee = 0;
    } else if (hasFrozen) {
        shipping_fee = frozen_shipping_fee;
    } else {
        shipping_fee = basic_shipping_fee;
    }
    itemName += "運費: $" + shipping_fee;
    totalPrice += shipping_fee;
    let base_param = {
        MerchantTradeNo: orderId,
        MerchantTradeDate: date, //ex: 2017/02/13 15:45:30
        TotalAmount: "" + totalPrice,
        TradeDesc: '上海火腿購買',
        ItemName: itemName,
        ReturnURL: 'http://68.183.177.114/payment_res_atm_res',
        EncryptType: '1',
        // ChooseSubPayment: '',
        // OrderResultURL: 'http://68.183.177.114/#!/profile',
        // NeedExtraPaidInfo: '1',
        ClientBackURL: 'http://68.183.177.114/#!/profile',
        // ItemURL: 'http://item.test.tw',
        // Remark: '交易備註',
        // StoreID: '',
        // CustomField1: '',
        // CustomField2: '',
        // CustomField3: '',
        // CustomField4: ''
    };

    // 若要測試開立電子發票，請將inv_params內的"所有"參數取消註解 //
    let inv_params = {
        // RelateNumber: 'PLEASE MODIFY',  //請帶30碼uid ex: SJDFJGH24FJIL97G73653XM0VOMS4K
        // CustomerID: 'MEM_0000001',  //會員編號
        // CustomerIdentifier: '',   //統一編號
        // CustomerName: '測試買家',
        // CustomerAddr: '測試用地址',
        // CustomerPhone: '0123456789',
        // CustomerEmail: 'johndoe@test.com',
        // ClearanceMark: '2',
        // TaxType: '1',
        // CarruerType: '',
        // CarruerNum: '',
        // Donation: '2',
        // LoveCode: '',
        // Print: '1',
        // InvoiceItemName: '測試商品1|測試商品2',
        // InvoiceItemCount: '2|3',
        // InvoiceItemWord: '個|包',
        // InvoiceItemPrice: '35|10',
        // InvoiceItemTaxType: '1|1',
        // InvoiceRemark: '測試商品1的說明|測試商品2的說明',
        // DelayDay: '0',
        // InvType: '07'
    };
    let pay_info_url = 'http://68.183.177.114/payment_res_atm';
    let exp = '7';
    let cli_redir_url = '';

    let create = new ecpay_payment();
    let htm = create.payment_client.aio_check_out_atm(parameters = base_param, url_return_payinfo = pay_info_url, exp_period = exp, client_redirect = cli_redir_url, invoice = inv_params);
    res.send(htm);
})


function _uuid() {
    var d = Date.now();
    if (typeof performance !== 'undefined' && typeof performance.now === 'function') {
        d += performance.now(); //use high-precision timer if available
    }
    return 'xxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
}

function sendEmail(toEmail, emailTitle, emailMsg) {
    var transporter = nodemailer.createTransport({
        service: 'gmail',

        auth: {
            user: "shanhihamonlinestore",
            pass: '2020shanhihamonlinestore!'
        }
    });

    var mailOptions = {
        from: 'shanhihamonlinestore@gmail.com',
        to: toEmail,
        subject: emailTitle,
        html: emailMsg
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            return false;
        } else {
            return true;
        }
    });
}

function NOW() {

    var date = new Date();
    var aaaa = date.getFullYear();
    var gg = date.getDate();
    var mm = (date.getMonth() + 1);

    if (gg < 10)
        gg = "0" + gg;

    if (mm < 10)
        mm = "0" + mm;

    var cur_day = aaaa + "/" + mm + "/" + gg;

    var hours = date.getHours()
    var minutes = date.getMinutes()
    var seconds = date.getSeconds();

    if (hours < 10)
        hours = "0" + hours;

    if (minutes < 10)
        minutes = "0" + minutes;

    if (seconds < 10)
        seconds = "0" + seconds;

    return cur_day + " " + hours + ":" + minutes + ":" + seconds;

}

function formatBirthdayDate() {
    var d = new Date(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [month, day].join('-');
}

function send_birthday_credit() {
    const rows = db.prepare("SELECT * FROM CREDITS").all();
    let birthdayCreditRule = rows[0];
    let isEnable = birthdayCreditRule.birthday_credit_enable;
    if (isEnable == 1) {
        let todayDate = formatBirthdayDate();
        let lastGiftDate = birthdayCreditRule.last_gift_date;
        if (todayDate != lastGiftDate) {
            let birthdayCredit = birthdayCreditRule.birthday_credit;
            const userRows = db.prepare("SELECT ID,CREDIT FROM USER WHERE BIRTHDAY LIKE '%" + todayDate + "'").all();
            for (let i = 0; i < userRows.length; i++) {
                let updateCredit = userRows[i].credit + birthdayCredit;
                let data = [updateCredit, userRows[i].id];
                const stmt = db.prepare('UPDATE USER SET CREDIT = ? WHERE ID =?');
                stmt.run(data);
            }
            let creditDate = [todayDate, 1];
            const bstmt = db.prepare('UPDATE CREDITS SET LAST_GIFT_DATE = ? WHERE ID =?');
            bstmt.run(creditDate);
        }

    }

}
function base64MimeType(encoded) {
    var result = null;

    if (typeof encoded !== 'string') {
        return result;
    }

    var mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);

    if (mime && mime.length) {
        result = mime[1];
    }
    return result.replace("image/", "");;
}
//send_birthday_credit()

function extendSession(req) {
    req.session._garbage = Date();
    req.session.touch();
}

app.listen(port, () => console.log(`Example app listening on port ${port}!`))