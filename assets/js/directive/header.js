app.directive("chiperHeader", function() {
    return {
        template : 
        '<header class="top-navbar" ng-controller="headerController">'+

        '<nav class="navbar navbar-expand-lg navbar-light bg-light">'+
          '<div class="container">'+
            '<a class="navbar-brand" href="index.html">'+
              '<img class="header-logo" src="images/logo.png" alt="" />'+
            '</a>'+
            '<button class="navbar-toggler" id="header-btn" type="button" data-toggle="collapse" data-target="#navbars-rs-food" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">'+
              '<span class="navbar-toggler-icon"></span>'+
            '</button>'+
            '<div class="collapse navbar-collapse sh-menu" id="navbars-rs-food">'+
              '<ul class="navbar-nav ml-auto" ng-show="showMenu">'+
                '<li class="nav-item" id="menuHome" ><a class="nav-link" href="#!home">首頁</a></li>'+
                '<li class="nav-item" id="menuProduct" ><a class="nav-link" href="#!product">精選商品</a></li>'+
                '<li class="nav-item" id="menuRecipe"><a class="nav-link" href="#!recipe">美味食譜</a></li>'+
                '<li class="nav-item" id="menuMedia"><a class="nav-link" href="#!media">媒體報導</a></li>'+
               ' <li class="nav-item" id="menuContact"><a class="nav-link" href="#!contact">聯絡我們</a></li>'+
               ' <li class="nav-item" id="menuLocation"><a class="nav-link" href="#!location">地點位置</a></li>'+
               ' <li class="nav-item non-member" ><a class="nav-link" href="javascript:void(0);" ng-click="registerPopup()"><i class="fas fa-user-plus"></i>註冊</a></li>'+
               ' <li class="nav-item non-member" ><a class="nav-link" href="javascript:void(0);" ng-click="signinPopup()"><i class="fas fa-sign-in-alt"></i>登入</a></li>'+
               ' <li class="nav-item member" id="menuProfile" ><a class="nav-link"  href="#!profile"><i class="fas fa-user"></i><span>{{userName}}</span></a></li>'+
               ' <li class="nav-item admin" id="menuAdmin"><a class="nav-link"  href="#!admin"><i class="fas fa-user-cog"></i><span>後台管理</span></a></li>'+
               ' <li class="nav-item member" ><a class="nav-link"  id="menuCart" href="javascript:void(0);"  ng-click="showCart()"><i class="fas fa-shopping-cart"></i>購物車</a></li>'+
               ' <li class="nav-item member admin" ><a class="nav-link" href="javascript:void(0);" ng-click="signout()"><i class="fas fa-door-open"></i>登出</a></li>'+
              '</ul>'+
            '</div>'+
          '</div>'+
       ' </nav>'+
       '<a href="javascript:void(0);" ng-click="showNews()" id="news-icon"><i class="fa fa-newspaper-o"></i></a>'+

      '</header>'
    };
});