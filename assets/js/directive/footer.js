app.directive("chiperFooter", function() {
    return {
        template : 
        '<footer class="footer-area bg-f" ng-controller="footerController">'+
        '<div class="container">'+
          '<div class="row">'+
            '<div class="col-lg-6 col-md-6">'+
              '<h3>營業時間</h3>'+
              '<p><span class="text-color">星期一 : </span>休市</p>'+
              '<p><span class="text-color">星期二 ~ 星期日 :</span> 早上8點到晚上6點</p>'+
            
            '</div>'+
            '<div class="col-lg-6 col-md-6">'+
              '<h3>聯絡資訊</h3>'+
              '<p class="lead">地址：{{infoData.address}} </p>'+
              '<p class="lead">服務專線：{{infoData.phone}} 傳真：{{infoData.tax}} </p>'+
              '<p class="lead">E-mail: {{infoData.email}} </p>'+
              // // '<p class="lead">Line: @line </p>'+
              // '<p class="lead">'+
              // '<a class="m-3 footer-icon facebook-color" href="https://www.facebook.com/sanhiham" taeget="_blank" ><i class="fa fa-facebook-square" aria-hidden="true"></i></a>'+
              // // '<a class="m-3 footer-icon line-color" href="#"><i class="fab fa-line" aria-hidden="true"></i></a>'+
              // '<a class="m-3 footer-icon" href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></p>'+

            '</div>'+
          '</div>'+
        '</div>'+
        '<div class="copyright">'+
         ' <div class="container">'+
            '<div class="row">'+
              '<div class="col-lg-12">'+
                '<p class="company-name">Copyright © 2020 sanhiham 上海火腿食品 劉福生號'+
              '</div>'+
            '</div>'+
          '</div>'+
        '</div>'+
        
      '</footer>'
    };
});