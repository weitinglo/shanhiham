
app.factory("mediaService", function ($http) {
    return {
        getMedia: function (type) {
            return $http.get('media')
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },
        deleteMedia: function (id) {
            return $http.delete('delete_media', { params: { id: id } })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        updateMedia: function (mediaJsonData) {
            return $http.post('update_media', {
                media:mediaJsonData
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        addMedia: function (mediaJsonData) {
            return $http.post('add_media', {
                media:mediaJsonData
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },
    }

})
