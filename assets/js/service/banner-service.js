
app.factory("bannerService", function ($http) {
    return {
        getBanner: function () {
            return $http.get('banner')
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
       
        updateBanner:function(banner){
            return $http.post('update_banner', {
                banner:banner
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        }

    }
})
