
app.factory("recipeService", function ($http) {
    return {
        // Home/History page
        getRecipe: function (type) {
            return $http.get('recipe')
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },
        deleteRecipe: function (id) {
            return $http.delete('delete_recipe', { params: { id: id } })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        updateRecipe: function (recipeJsonData) {
            return $http.post('update_recipe', {
                recipe:recipeJsonData
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        addRecipe: function (recipeJsonData) {
            return $http.post('add_recipe', {
                recipe:recipeJsonData
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },
    }

})
