
app.factory("userService", function ($http) {
    return {
        sendNewPassword: function (email) {
            return $http.post('send_new_password', {
                email:email
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },
        updateUserProfile: function (userData) {
            return $http.post('update_user', {
                userData:userData
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },
        getUserList : function () {
            return $http.get('user_list')
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        getCredits : function () {
            return $http.get('credits')
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        updateCredits : function (credit) {
            return $http.post('update_credit', {
                credit:credit
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },
        getUserById : function(userId){
            return $http.get('user_by_id?userId='+userId)
            .then(function (response) {
                return response.data;
            }, function (response) {
                return response;
            });
        }
    }
})
