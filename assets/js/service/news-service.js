
app.factory("newsService", function ($http) {
    return {
        getNews: function () {
            return $http.get('news')
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        deleteNews: function (id) {
            return $http.delete('delete_news', { params: { id: id } })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        addNews: function (newsJson) {
            return $http.post('add_news', {
                news: newsJson
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },
        updateNewsShow:function(id,status){
            return $http.post('update_news_show', {
                id:id,
                status:status
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },
        updateNews:function(newsJson){
            return $http.post('update_news', {
                news:newsJson
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        }

    }
})
