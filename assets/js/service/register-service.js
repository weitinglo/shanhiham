
app.factory("registerService", function ($http) {
    return {
        registerUser: function (dataJson) {
            return $http.post('register_user', {
                'name': dataJson.name,
                'email': dataJson.email,
                'phone': dataJson.phone,
                'birthday': dataJson.birthday,
                'password': dataJson.password
            })
            .then(function (response) {
                return response.data;
            }, function (response) {
                return "Something went wrong";
            });
        }
    }

})
