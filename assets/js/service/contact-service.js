
app.factory("contactService", function ($http) {
    return {
        sendComment: function (dataJson) {
            return $http.post('send_comment', {
                'comment': dataJson
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },
        getComment: function (type) {
            return $http.get('comment')
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        }
    }

})
