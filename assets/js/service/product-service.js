
app.factory("productService", function ($http) {
    return {
        // Home/History page
        getProduct: function (type) {
            return $http.get('product?type='+type)
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },
        getProductType: function (type) {
            return $http.get('product_type')
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },
        getProductMenu: function () {
            return $http.get('product_menu')
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },
        updateProduct: function (productJsonData) {
            return $http.post('update_product', {
                product:productJsonData
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        addProduct: function (productJsonData) {
            return $http.post('add_product', {
                product:productJsonData
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    console.log(response)
                    return response;
                });
        },
        deleteProduct: function (productId) {
            return $http.delete('delete_product', {params: {id: productId}})
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        updateProductOrder: function(id,order){
            return $http.post('update_product_order', {
                id:id,
                order:order
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },
        updateProductTypeOrder : function(order,type){
            return $http.post('update_product_type_order', {
                type:type,
                order:order
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },
        updateFrozen: function (id,val) {
            return $http.post('update_frozen_product', {
                id:id,
                val:val
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },
        updateMenu: function (file) {
            return $http.post('update_menu', {
                file:file
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        }
    }

})
