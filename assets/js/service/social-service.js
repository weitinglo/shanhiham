
app.factory("socialService", function ($http) {
    return {
        getSocial: function () {
            return $http.get('social')
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
       
        updateSocial:function(social){
            return $http.post('update_social', {
                social:social
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        }

    }
})
