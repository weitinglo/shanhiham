
app.factory("orderService", function ($http) {
    return {
        saveOrder: function (dataJson,city,address,postcode,price) {
            return $http.post('save_order', {
                'order': dataJson,
                'city' : city,
                'address' : address,
                'postcode' : postcode,
                'price' :price
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },
        getOrder: function () {
            return $http.get('order')
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },
        getOrderOnId: function (orderId) {
            return $http.get('order_on_id?orderId=' + orderId)
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },
        getOrderList: function (userId) {
            return $http.get('order_list')
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },
        getOrderListOnId: function(orderId){
            return $http.get('order_list_on_id?orderId=' + orderId)
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },
        updateOrderStatus: function (id,status) {
            return $http.post('update_order_status', {
                'id': id,
                'status': status
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },
        pay: function (paymentJson) {
            return $http.post('pay', {
                'paymentJson': paymentJson
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },
        deleteOrder: function(id){
            return $http.delete('delete_order', {params: {id: id}})
            .then(function (response) {
                return response.data;
            }, function (response) {
                return response;
            });
        },
        
        
    }

})
