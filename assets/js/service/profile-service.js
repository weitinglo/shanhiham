
app.factory("profileService", function ($http) {
    return {
        getProfile: function (type) {
            return $http.get('profile')
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },
        repay:function(data){
            return $http.post('repay', {
                'data': data
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        }
    }

})
