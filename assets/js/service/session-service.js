
app.factory("sessionService", function ($http) {
    return {
        // Home/History page
        checkLogin: function () {
            return $http.get('check_login')
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },   
        checkAdmin: function () {
            return $http.get('check_admin')
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },   
        userData: function () {
            return $http.get('user_data')
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },
        signin: function (dataJson) {
            return $http.post('signin', {
                'email': dataJson.email,        
                'password': dataJson.password
            })
            .then(function (response) {
                return response.data;
            }, function (response) {
                return "Something went wrong";
            });
        },
        signout: function (dataJson) {
            return $http.post('signout', {})
            .then(function (response) {
                return response.data;
            }, function (response) {
                return "Something went wrong";
            });
        }
    }

})
