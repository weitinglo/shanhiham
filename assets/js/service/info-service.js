
app.factory("infoService", function ($http) {
    return {
        getInfo: function (type) {
            return $http.get('info')
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },
        updateInfo: function (infoJson) {
            return $http.post('update_info', {
                info:infoJson
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        }
    }

})
