
app.factory("rulesService", function ($http) {
    return {
        getRules: function (type) {
            return $http.get('rules')
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },
        updateNotice: function (noticeJson) {
            return $http.post('update_notice', {
                rule:noticeJson
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return "Something went wrong";
                });
        },
    }

})
