app.config(['$routeProvider',
function ($routeProvider) {
  $routeProvider
    .when('/home', {
      templateUrl: '/home.htm',
      controller: 'homeController'
    })
    .when('/contact', {
      templateUrl: '/contact.htm',
      controller: 'contactController'
    })
    .when('/location', {
      templateUrl: '/location.htm',
      controller: 'locationController'
    })
    .when('/media', {
      templateUrl: '/media.htm',
      controller: 'mediaController'
    })
    .when('/product', {
      templateUrl: '/product.htm',
      controller: 'productController'
    })
    .when('/recipe', {
      templateUrl: '/recipe.htm',
      controller: 'recipeController'
    })
    .when('/profile', {
      templateUrl: '/profile.htm',
      controller: 'profileController'
    })
    .when('/cart', {
      templateUrl: '/cart.htm',
      controller: 'cartController'
    })
    .when('/order', {
      templateUrl: '/order.htm',
      controller: 'orderController'
    })
    .when('/admin', {
      templateUrl: '/admin.htm',
      controller: 'adminController'
    })
    .when('/admin-order', {
      templateUrl: '/admin-order.htm',
      controller: 'adminController'
    })
    .when('/admin-product', {
      templateUrl: '/admin-product.htm',
      controller: 'adminController'
    })
    .when('/admin-product-order', {
      templateUrl: '/admin-product-order.htm',
      controller: 'adminController'
    })
    .when('/admin-rule', {
      templateUrl: '/admin-rule.htm',
      controller: 'adminController'
    })
    .when('/admin-comment', {
      templateUrl: '/admin-comment.htm',
      controller: 'adminController'
    })
    .when('/admin-user', {
      templateUrl: '/admin-user.htm',
      controller: 'adminController'
    })
    .when('/admin-news', {
      templateUrl: '/admin-news.htm',
      controller: 'adminController'
    })
    .when('/admin-recipe', {
      templateUrl: '/admin-recipe.htm',
      controller: 'adminController'
    })
    .when('/admin-social', {
      templateUrl: '/admin-social.htm',
      controller: 'adminController'
    })
    .when('/admin-media', {
      templateUrl: '/admin-media.htm',
      controller: 'adminController'
    })
    .when('/admin-banner', {
      templateUrl: '/admin-banner.htm',
      controller: 'adminController'
    })
    .when('/print-product', {
      templateUrl: '/print-product.htm',
      controller: 'printProductController'
    })
    .when('/product-detail', {
      templateUrl: '/product-detail.htm',
      controller: 'printProductController'
    })
    .otherwise({
      redirectTo: '/home'
    });
}]);