app.controller('headerController', function ($scope, $uibModal,sessionService,$location,newsService,$uibModalStack) {
  $scope.userName='';
  $scope.showMenu = false;
  $scope.registerPopup = function () {
    var modalInstance = $uibModal.open({
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'registerModalCotent.html',
      controller: 'registerModalInstanceCtrl',
      controllerAs: '$scope',
      size: "lg",
      appendTo: "",
      resolve: {
        items: function () {
          return "";
        }
      }
    });
  }
  $scope.showNews = function(){
    
    newsService.getNews().then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else {
        for(let i=0;i<data.length;i++){
          if(data[i].show==1)
            notifyNews(data[i])
        }

      }
    });

  }
  $scope.signinPopup = function () {
    var modalInstance = $uibModal.open({
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'signinModalCotent.html',
      controller: 'signinModalInstanceCtrl',
      controllerAs: '$scope',
      size: "lg",
      appendTo: "",
      resolve: {
        items: function () {
          return "";
        }
      }
    });
  }

  $scope.signout = function(){
    sessionService.signout().then(function (data) {
      if(data.code==200){
        window.location.href = "/";
      }else{
        alert(data.msg)
      }
    });
  }
  $scope.showCart = function(){
    var modalInstance = $uibModal.open({
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'cartModalCotent.html',
      controller: 'cartModalInstanceCtrl',
      controllerAs: '$scope',
      size: "lg",
      appendTo: "",
      resolve: {
        items: function () {
          return "";
        }
      }
    });
  }
  $scope.sessionCheck = function () {
    $scope.userName='';
    sessionService.checkLogin().then(function (data) {
        if(data.login==false){
          $(".non-member").css("display","inline")
          $(".member").css("display","none")
        }else{
          sessionService.userData().then(function (userDataRes) {
            if(userDataRes.level!=-1){
              $scope.userLevel = userDataRes.level;
              $scope.userName = userDataRes.name;
              $scope.userEmail = userDataRes.email;
              $scope.userId = userDataRes.id;
            }
            if($scope.userLevel==0){
              $(".non-member").css("display","none")
              $(".member").css("display","none")
              $(".admin").css("display","inline")
  
            }else{
              $(".non-member").css("display","none")
              $(".member").css("display","inline")
            }
           
          });

        }
        $scope.showMenu=true;
    });

    
}
  window.onhashchange = function() {
    $uibModalStack.dismissAll();
  }

  $scope.init=function(){ 
    $scope.sessionCheck();
  }
  $scope.init();
});

app.controller('registerModalInstanceCtrl', function ($scope, $uibModalInstance, registerService) {
  $scope.registerName = '';
  $scope.registerEmail = '';
  $scope.registerPhone = '';
  $scope.registerPassword = '';
  $scope.registerBirthday = '';
  $scope.registerPasswordConfirm = '';
  $scope.errorMsg = '';
  $scope.successMsg = '';
  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
  $scope.register = function () {
    $scope.errorMsg = '';
    $scope.successMsg = '';
    if ($scope.registerPassword != $scope.registerPasswordConfirm)
      $scope.errorMsg = "確認密碼與密碼不相同";
    if (!$scope.registerPasswordConfirm)
      $scope.errorMsg = "請輸入您的確認密碼";
    if ($scope.registerPassword.length < 5)
      $scope.errorMsg = "密碼長度必須要5位以上";
    if (!$scope.registerPassword)
      $scope.errorMsg = "請輸入您的密碼";
    if(!$scope.registerBirthday)
      $scope.errorMsg = "請輸入您的出生日期"
    if (!$scope.registerPhone)
      $scope.errorMsg = "請輸入您的電話號碼";
    if (!$scope.registerEmail)
      $scope.errorMsg = "請輸入您的電子信箱";
    if (!$scope.registerName)
      $scope.errorMsg = "請輸入您的姓名";
    if($scope.errorMsg!='')
      return;
    let dataJson ={
      name:$scope.registerName,
      email:$scope.registerEmail,
      phone:$scope.registerPhone,
      birthday:formatBirthdayDate($scope.registerBirthday),
      password:$scope.registerPassword
    }
    registerService.registerUser(dataJson).then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else {
        $scope.successMsg = "註冊成功！";
        window.location.href = "/";      }
    });
  }

  $scope.registerKey = function(event){
    if (event.keyCode === 13) {
      $scope.register();
    }
  }
  
});

app.controller('signinModalInstanceCtrl', function ($scope,$uibModal, $uibModalInstance, sessionService) {
  $scope.email = '';
  $scope.password = '';
  $scope.errorMsg = '';
  $scope.successMsg = '';
  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
  $scope.toRegister = function(){
    $uibModalInstance.dismiss('cancel');
    $scope.registerPopup();
  }
  $scope.registerPopup = function () {
    var modalInstance = $uibModal.open({
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'registerModalCotent.html',
      controller: 'registerModalInstanceCtrl',
      controllerAs: '$scope',
      size: "lg",
      appendTo: "",
      resolve: {
        items: function () {
          return "";
        }
      }
    });
  }
  $scope.signin = function () {
    $scope.errorMsg = '';
    $scope.successMsg = '';
    if (!$scope.password)
      $scope.errorMsg = "請輸入您的密碼";
    if (!$scope.email)
      $scope.errorMsg = "請輸入您的電子信箱";
    if($scope.errorMsg!='')
      return;
    let dataJson ={
      email:$scope.email,
      password:$scope.password
    }
    sessionService.signin(dataJson).then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else {
        $scope.successMsg = data.msg;
        location.reload();
      }
    });
  }
  $scope.forgetPasswordPopup= function () {
    $uibModal.open({
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'forgetPasswordModalCotent.html',
      controller: 'forgetPasswordModalCotentCtrl',
      controllerAs: '$scope',
      size: "lg",
      appendTo: "",
      resolve: {
        items: function () {
          return "";
        }
      }
    });
  }
  
  $scope.forgetPassword = function(){
    $uibModalInstance.dismiss('cancel');
    $scope.forgetPasswordPopup();
  }
  $scope.signinKey = function(event){
    if (event.keyCode === 13) {
      $scope.signin();
    }
  }

});