app.controller('forgetPasswordModalCotentCtrl', function ($scope,userService, $uibModalInstance) {
  $scope.email = '';
  $scope.password = '';
  $scope.errorMsg = '';
  $scope.successMsg = '';
  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
  $scope.toRegister = function(){
    $uibModalInstance.dismiss('cancel');
    $scope.registerPopup();
  }
  
  $scope.reset = function () {
    $scope.errorMsg = '';
    $scope.successMsg = '';
    if (!$scope.email)
      $scope.errorMsg = "請輸入您的電子信箱";
    if($scope.errorMsg!='')
      return;
    let dataJson ={
      email:$scope.email,
    }
    userService.sendNewPassword($scope.email).then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else {
        $scope.successMsg = data.msg;
       
      }
    });
  }

});