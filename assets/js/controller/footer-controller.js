app.controller('footerController', function ($scope, infoService) {
  $scope.infoData=[];
  $scope.getInfo = function(){
    infoService.getInfo().then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else {
        $scope.infoData = data;
      }
    });
  }
  $scope.init = function () {
    $scope.getInfo();
  }
  $scope.init();
});
