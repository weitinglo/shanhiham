
app.controller('profileController', function ($scope, $uibModal, sessionService, orderService, profileService, userService, NgTableParams) {
  $scope.orderTableList = [];
  $scope.orderListTableData = [];
  $scope.profileData = [];
  $scope.tableParams = '';
  $scope.address="";
  $scope.sessionCheck = function () {
    sessionService.checkLogin().then(function (data) {
      if (data.login == false)
        window.location.href = "/";
    });
  }
  $scope.updateUser = function (userData) {
    $scope.successMsg = '';
    $scope.errorMsg = '';
    if (!$scope.profileData.birthday || !$scope.profileData.birthday instanceof Date) {
      notifyWarning("請輸入生日");
      return;
    } else {
      $scope.profileData.birthday = formatBirthdayDate($scope.profileData.birthday);
    }
    userService.updateUserProfile($scope.profileData).then(function (data) {
      if (data.code && data.code != 200) {
        notifyWarning(data.msg);
      } else {
        notifyInfo(data.msg);
      }
    });
  }
  $scope.getOrderList = function () {
    orderService.getOrderList().then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else {
        for(let i=0;i<data.length;i++){
          let payStatus = data[i].pay_status;
          let payType = data[i].pay_type;
          if(payType=='ATM'){
            if(payStatus==2){
              data[i].pay_status_text = "客戶已取得帳號";
              data[i].pay_status_code = 2;
            }if(payStatus==1){
              data[i].pay_status_text = "已付款";
              data[i].pay_status_code = 0;
            }else{
              data[i].pay_status_text = "客戶未取得帳號";
              data[i].pay_status_code = 3;
            }
          }else{
            if(payStatus==1){
              data[i].pay_status_text = "已付款";
              data[i].pay_status_code = 0;
            }else{
              data[i].pay_status_text = "未付款";
              data[i].pay_status_code = 1;
            }
          }
        }
        $scope.orderListTableData = data;
        $scope.tableParams = new NgTableParams({}, { dataset: data });

      }
    });
  }

  $scope.getProfile = function () {
    $scope.successMsg = '';
    $scope.errorMsg = '';
    profileService.getProfile().then(function (data) {
      if (data.code && data.code != 200) {
        notifyWarning(data.msg);
      } else {
        $scope.profileData = data;
        if( $scope.profileData.city){
          let city = $scope.profileData.city;
          let area = $scope.profileData.postcode.split(" ")[1];
          let postcode = $scope.profileData.postcode.split(" ")[0];
          let address = $scope.profileData.address;
          $scope.address = city+area+address+" "+postcode;
        }
        if ($scope.profileData.birthday)
          $scope.profileData.birthday = new Date($scope.profileData.birthday);
      }
    });
  }
  $scope.orderPopup = function (data) {
    var modalInstance = $uibModal.open({
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'orderDetailModalCotent.html',
      controller: 'adminModalInstanceCtrl',
      controllerAs: '$scope',
      size: "lg",
      appendTo: "",
      resolve: {
        page: function () {
          return 'order';
        },
        title: function () {
          return null;
        },
        message: function () {
          return null;
        },
        orderId: function () {
          return data.order_id;;
        }
      }
    });
  }
  $scope.repay = function(data){
    profileService.repay(data).then(function (data) {
      if (data.code && data.code != 200) {
        notifyWarning(data.msg);
      } else {
        window.location.href = "/pay?orderId="+data.new_order_id;

        //$scope.getOrderList();
      }
    });
  }

  $scope.atmInfo = function (data) {
    var modalInstance = $uibModal.open({
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'atmDetailModalCotent.html',
      controller: 'adminModalInstanceCtrl',
      controllerAs: '$scope',
      size: "lg",
      appendTo: "",
      resolve: {
        page: function () {
          return 'order';
        },
        title: function () {
          return null;
        },
        message: function () {
          return data;
        },
        orderId: function () {
          return data;;
        }
      }
    });
  }

  
  $scope.init = function () {
    $scope.sessionCheck();
    $scope.getProfile();
    $scope.getOrderList();
    updateMenu('profile')
  }
  $scope.init();
});

