
app.controller('productController', function ($scope, productService, $uibModal,$anchorScroll,  $location ,sessionService,$rootScope) {
    $rootScope.loadingPage = true;
    updateMenu('product');
    $scope.type = '';
    $scope.search='';
    $scope.productTableList = [];
    $scope.hamTableList = [];
    $scope.fishTableList = [];
    $scope.sausageTableList = [];
    $scope.othersTableList = [];
    $scope.frozenTableList = [];
    $scope.nutsTableList = [];
    $scope.savedItem = [];
    $scope.productMap =  new Map();
    $scope.messageForIE = "";
    let isIE = isIEcheck();
    if(isIE){
        $scope.messageForIE = "請使用Chrome 或是 Firefox 來取得更好的購物體驗喔";

    }

    $('select').on('change', function (e) {
        let index = document.getElementById("categoty-select").value;
        $('html, body').animate({
            scrollTop: $("#anchor"+index).offset().top-150
        }, 1000);

    });

    $scope.getProduct = function () {
        productService.getProduct($scope.type).then(function (data) {
            if (data.code && data.code != 200) {
                //nowuiDashboard.showErrorNotification('top', 'center', data.msg)
            } else {
                for (let i = 0; i < data.length; i++) {
                    
                    if(data[i].d_price=="" || data[i].d_price==null)
                        data[i].d_price="";
                    let type = data[i].type;
                    if($scope.productMap.get(type)){
                        $scope.productMap.get(type).push(data[i]);
                    }else{
                        let pArr = new Array();
                        pArr.push(data[i]);
                        $scope.productMap.set(type,pArr)
                    }
                }
            }
        });
    }
    $scope.signinPopup = function () {
        var modalInstance = $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'signinModalCotent.html',
            controller: 'signinModalInstanceCtrl',
            controllerAs: '$scope',
            size: "lg",
            appendTo: "",
            resolve: {
                items: function () {
                    return "";
                }
            }
        });
    }

    $scope.addToCartsessionCheck = function (product) {
        sessionService.checkLogin().then(function (data) {
            if (data.login == false)
                $scope.signinPopup();
            else{
                $scope.addToCart(product);
            }
        });
    }
   

    $scope.addToCart = function (data) {
        let price = 0;
        if(data.d_price!=""){
            price = data.d_price;
        }else{
            price = data.price;
        }
        let productJson = {
            id: data.id,
            name: data.name,
            img:data.img,
            price: price,
            amount:data.amount
        }
        savedItems = getProductinCookie();
        let product_cookie_arr = [];
        try{
            fbq('track', 'AddToCart');
        }catch(err){
            console.log("FB Pixel Problem");
        }
        if (!savedItems) {
            product_cookie_arr.push(productJson);
            $.cookie("product", JSON.stringify(product_cookie_arr));
        } else {
            product_cookie_arr = savedItems;
            let existed = false;
            for (i in savedItems) {
                if (savedItems[i].id == data.id){
                    existed = true;
                    product_cookie_arr[i].amount=data.amount;
                }
                    
            }
            if (!existed)
                product_cookie_arr.push(productJson);
            $.cookie("product", JSON.stringify(product_cookie_arr));
        }
        notifyInfo(data.name+ " 已成功加入購物車")
        $("#menuCart").addClass("run-animation");
    };

    $scope.popup = function (data) {
        var modalInstance = $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'productModalCotent.html',
            controller: 'productModalInstanceCtrl',
            controllerAs: '$scope',
            size: "lg",
            appendTo: "",
            resolve: {
                items: function () {
                    return data;
                }
            }
        });
    }
   
    $scope.displaySoppingCart = function () {
        let savedItem = getProductinCookie()
        if (savedItem) {
            for (i in savedItem) {
                let product = savedItem[i];
                $scope.addToCart(product.name, product.price, product.img, product.amout, true);
            }
        }
    }

  $scope.getProductMenu = function () {
    

    $scope.menuPath="";
    productService.getProductMenu().then(function (data) {
      if (data.code && data.code != 200) {
        notifyWarning(data.msg);
      } else {
        $scope.menuPath = data[0].path;
       
      }
      $rootScope.loadingPage = false;
    });
  }

    $scope.init = function () {
        $scope.getProduct();
        $scope.getProductMenu();
        // $scope.displaySoppingCart();
    }
    $scope.init();
});

app.controller('productModalInstanceCtrl', function ($scope, $uibModalInstance, items) {
    $scope.items = items;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };


});