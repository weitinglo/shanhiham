
app.controller('printProductController', function ($scope, $location, orderService, userService, rulesService) {
    $scope.orderId = $location.search().id;
    $scope.shipping = [];
    $scope.orderListTableData = [];
    $scope.ruleData = [];
    $scope.userInfo = {};
    $scope.userId = ""
    $scope.totalProductPrice = 0;
    $scope.finalPrice = 0;
    $scope.hasFrozenProduct = 0;
    $scope.shippingFee = 0;
    $scope.orderListData={};
    $scope.deliverAddress="";
    $scope.readyCode =0;
    /*3*/
    $scope.getUserById = function (userId) {
        userService.getUserById(userId).then(function (data) {
            if (data.code && data.code != 200) {
                notifyWarning(data.msg)
            } else {
                $scope.userInfo = data[0];
                $scope.readyCode+=1;

            }
        });
    }
    /*2*/
    $scope.getOrderOnId = function (orderId) {
        orderService.getOrderOnId(orderId).then(function (data) {
            if (data.code && data.code != 200) {
                notifyWarning(data.msg)
            } else {
                $scope.orderListTableData = data;
                if (data.length > 0) {
                    for (let i = 0; i < data.length; i++) {
                        data[i].total = data[i].product_amout * data[i].product_price;
                        $scope.totalProductPrice += data[i].total;
                        $scope.hasFrozenProduct += data[i].frozen;
                    }
                    $scope.finalPrice = $scope.totalProductPrice;
                    if ($scope.finalPrice < $scope.ruleData.free_shipping_price) {
                        if ($scope.hasFrozenProduct > 0) {
                            $scope.finalPrice += $scope.ruleData.frozen_shipping_fee;
                            $scope.shippingFee = $scope.ruleData.frozen_shipping_fee;
                        } else {
                            $scope.finalPrice += $scope.ruleData.shipping_fee;
                            $scope.shippingFee = $scope.ruleData.shipping_fee;
                        }
                    }
                    $scope.getUserById(data[0].user_id)
                    $scope.readyCode+=1;
                }
            }
        });
    }

    /*1*/
    $scope.getRules = function () {
        rulesService.getRules('').then(function (data) {
            if (data.code && data.code != 200) {
                notifyWarning(data.msg)
            } else {
                $scope.ruleData = data;
                $scope.getOrderOnId($scope.orderId);
                $scope.readyCode+=1;

            }
        });
    }
    
    $scope.getOrder = function(){
        orderService.getOrderListOnId($scope.orderId).then(function (data) {
            if (data.code && data.code != 200) {
                notifyWarning(data.msg)
            } else {
                if(data.length>0){
                    let orderDetail = data[0];
                    for(let i=0; i<data.length;i++){
                        let payStatus = orderDetail.pay_status;
                        let payType =orderDetail.pay_type;
                        if(payType=='ATM'){
                            if(payStatus==2){
                                orderDetail.pay_status_text = "客戶已取得帳號";
                                orderDetail.pay_status_code = 2;
                            }else if(payStatus==1){
                                data[i].pay_status_text = "已付款";
                                data[i].pay_status_code = 0;
                              
                            }else{
                                orderDetail.pay_status_text = "客戶未取得帳號";
                              orderDetail.pay_status_code = 3;
                            }
                          }else{
                            if(payStatus==1){
                              orderDetail.pay_status_text = "已付款";
                              orderDetail.pay_status_code = 0;
                            }else{
                              orderDetail.pay_status_text = "未付款";
                              orderDetail.pay_status_code = 1;
                            }
                          }
                    }
                    $scope.orderListData = orderDetail;
                    let city = $scope.orderListData.city;
                    let postcode = $scope.orderListData.postcode.split(" ")[0];
                    let area = $scope.orderListData.postcode.split(" ")[1];
                    let address = $scope.orderListData.address;
                    $scope.deliverAddress = postcode+" " +city+area+address;
                    $scope.readyCode+=1;

                }
            }
        });
        
    }
    $scope.getRules();
    $scope.getOrder();
    var myVar = "";
    
    function print() {
        if( $scope.readyCode==4){
            window.print();
            myStopFunction();
        }
    }

    function myStopFunction() {
        clearInterval(myVar);
    }
    $scope.init = function(type){
        if(type=="print"){
            myVar= setInterval(print, 500);
        }
    }

});

