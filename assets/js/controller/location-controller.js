
app.controller('locationController', function ($scope,infoService,$rootScope) {
    $rootScope.loadingPage = true;

    updateMenu('location');
    $scope.infoData=[];
    $scope.getInfo = function(){
        infoService.getInfo().then(function (data) {
        if (data.code && data.code != 200) {
            $scope.errorMsg = data.msg;
        } else {
            $scope.infoData = data;
        }
        $rootScope.loadingPage = false;
        });
    }
    $scope.init = function () {
        $scope.getInfo();
    }
    $scope.init();

});
