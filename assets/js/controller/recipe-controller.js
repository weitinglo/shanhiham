
app.controller('recipeController', function ($scope, recipeService,$uibModal,$rootScope) {
    $rootScope.loadingPage = true;

    updateMenu('recipe');
    $scope.recipeTableData = [];
    $scope.getRecipe = function(){
        recipeService.getRecipe().then(function (data) {
            if (data.code && data.code != 200) {
                //nowuiDashboard.showErrorNotification('top', 'center', data.msg)
            } else {
                for(i in data){
                    let link = data[i].link;
                    if(link){
                     
                        if(link.indexOf("http")==-1){
                            data[i].link="http://"+data[i].link;
                        }
                    }
                }
                $scope.recipeTableData = data;
            }
            $rootScope.loadingPage = false;
        });
    }
    $scope.getRecipe();
    $scope.popup = function (data) {
       
        var modalInstance = $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'recipeModalCotent.html',
            controller: 'recipeModalInstanceCtrl',
            controllerAs: '$scope',
            size: "lg",
            appendTo: "",
            resolve: {
                items: function () {
                    return data;
                }
            }
        });
    }
});

app.controller('recipeModalInstanceCtrl', function ($scope, $uibModalInstance, items) {
    $scope.items = items;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };


});