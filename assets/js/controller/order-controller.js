

app.controller('orderController', function ($scope, orderService, sessionService, rulesService, productService,profileService, $uibModal,$rootScope) {
    $scope.successMsg = '';
    $scope.errorMsg = '';
    $scope.city = '';
    $scope.address = '';
    $scope.postcode = '';
    $scope.rules = {};
    $scope.userLevel = -1;
    $scope.userName = "";
    $scope.userEmail = "";
    $scope.userId = -1;
    $scope.shipping = 150;
    $scope.userCity = "";
    $scope.userPostcode = "";
    $scope.cityArea = {} ;
    $scope.sessionCheck = function () {
        sessionService.checkLogin().then(function (data) {
            if (data.login == false)
                window.location.href = "/";
        });
        return true;
    }
    

    $scope.getRules = function () {
        rulesService.getRules().then(function (ruleData) {
            $scope.rules = ruleData;
            productService.getProduct($scope.type).then(function (data) {
                let hasFrozen = 0;

                if (data.code && data.code != 200) {
                    $scope.errorMsg = "系統發生錯誤-product";
                } else {
                    for (let i = 0; i < data.length; i++) {
                        for (let j = 0; j < $scope.cartTableData.length; j++) {
                            if ($scope.cartTableData[j].id == data[i].id) {
                                if (data[i].frozen == 1)
                                    hasFrozen = 1;
                            }
                        }
                    }
                    $scope.calculatePrice($scope.cartTableData, ruleData, hasFrozen);
                }
            });
            $rootScope.loadingPage = false;

        });
    }
    $scope.cartTableData = [];
    $scope.totalPrice = 0;
    $scope.userName = '';
    $scope.userEmail = '';
    $scope.calculatePrice = function (product, rule, hasFrozen) {
        $scope.totalPrice = 0;
        for (i in product) {
            let price = product[i].price * product[i].amount;
            $scope.totalPrice += price;
        }
        if (hasFrozen == 1) {
            $scope.shipping = rule.frozen_shipping_fee;
        } else {
            $scope.shipping = rule.shipping_fee;
        }
        if ($scope.totalPrice >= rule.free_shipping_price) {
            $scope.shipping = 0;
        }
        $scope.totalPrice += $scope.shipping;
    }

    $scope.confirm = function (address,type) {
        $scope.successMsg = '';
        $scope.errorMsg = '';
        let orderedProduct = getProductinCookie();
        let city = $scope.cityArea.selectCity;
        let poscode = $scope.cityArea.cityCode + " " + $scope.cityArea.selectArea;
        if (!$scope.cityArea.selectCity || !address || !$scope.cityArea.selectArea) {
            $scope.errorMsg = '請完整填寫寄送地址';
            return;
        }
        if (orderedProduct) {
            try{
                fbq('track', 'InitiateCheckout');
            }catch(err){
                console.log("FB Pixel Problem");
            }
            $("#confirmBtn").css("display", "none");
            $("#confirmLoading ").css("display", "inline");
            orderService.saveOrder(orderedProduct, city, address, poscode, $scope.totalPrice).then(function (data) {
                if (data.code && data.code != 200) {
                    notifyWarning(data.msg)
                } else {
                    if(type=='card')
                        window.location.href = "/pay?orderId="+data.orderId;
                    else if(type=='atm')
                        window.location.href = "/pay_atm?orderId="+data.orderId;

                }
                $("#confirmLoading").css("display", "none");

            });
        } else {
            $scope.errorMsg = "沒有商品加入喔";
        }
    }
   
    $scope.signinPopup = function () {
        var modalInstance = $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'signinModalCotent.html',
            controller: 'signinModalInstanceCtrl',
            controllerAs: '$scope',
            size: "lg",
            appendTo: "",
            resolve: {
                items: function () {
                    return "";
                }
            }
        });
    }
    $scope.getSessionUserData = function () {
        sessionService.userData().then(function (userDataRes) {
            if (userDataRes.level != -1) {
                $scope.userLevel = userDataRes.level;
                $scope.userName = userDataRes.name;
                $scope.userEmail = userDataRes.email;
                $scope.userId = userDataRes.id;
                $scope.getUserData();
            }
        });
    }
    $scope.getUserData = function () {
        profileService.getProfile().then(function (data) {
            if (data.code && data.code != 200) {
                notifyWarning(data.msg);
            } else {
                if(data.city)
                    $scope.cityArea.selectCity = data.city;
                if(data.postcode){
                    $scope.cityArea.selectArea = data.postcode.split(" ")[1];
                    $scope.cityArea.cityCode = data.postcode.split(" ")[0];
                }
                    

                $scope.address = data.address;
            }
        });
    }
    $scope.init = function () {
        if ($scope.sessionCheck()) {
            $scope.cartTableData = getProductinCookie();
            $scope.getSessionUserData();
            $scope.getRules();
        }
    }
    $scope.init();

});

