
app.controller('cartModalInstanceCtrl', function ($scope, $uibModalInstance) {
    $scope.cartTableData=[];
    $scope.totalPrice=0;
    $scope.order=function(){
        $uibModalInstance.dismiss('cancel');
        location.href="#!order";
    }
    $scope.calculatePrice=function(product){
        $scope.totalPrice=0;
        for(i in product){
            let price = product[i].price*product[i].amount;
            $scope.totalPrice+=price;
        }
    }
    $scope.updateCount=function(data){
        if(updateProductCountCookie(data)){
            notifyInfo("更新成功");
            $scope.init();
        }else{
            notifyWarning("更新失敗");

        }
        
    }
    $scope.delete=function(data){
        deleteDataFromProductCookie(data);
        $scope.init();
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.init=function(){
        $scope.cartTableData = getProductinCookie();
        $scope.calculatePrice($scope.cartTableData);
        $("#menuCart").removeClass("run-animation");
    }
    $scope.init();
});

