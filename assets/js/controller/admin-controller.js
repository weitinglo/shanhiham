
app.controller('adminController', function ($scope, $rootScope, $uibModal, $routeParams, sessionService, orderService, productService, rulesService, infoService, contactService, socialService, userService, newsService, recipeService, mediaService, bannerService, NgTableParams) {
  updateMenu('admin');
  $scope.orderListTableData = [];
  $scope.tableParams = '';
  $scope.menuFile = null;
  $scope.productTypeMap = {};

  $scope.sessionCheck = function () {
    sessionService.checkLogin().then(function (data) {
      if (data.login == false || data.level != 0)
        window.location.href = "/";
    });
  }
  $scope.getOrderList = function () {
    let orderId = $routeParams.id;
    orderService.getOrderList().then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else {
        for (let i = 0; i < data.length; i++) {
          let payStatus = data[i].pay_status;
          let payType = data[i].pay_type;
          if (payType == 'ATM') {
            if (payStatus == 2) {
              data[i].pay_status_text = "客戶已取得帳號";
              data[i].pay_status_code = 2;
            } else if (payStatus == 1) {
              data[i].pay_status_text = "已付款";
              data[i].pay_status_code = 0;
            } else {
              data[i].pay_status_text = "客戶未取得帳號";
              data[i].pay_status_code = 3;
            }
          } else {
            if (payStatus == 1) {
              data[i].pay_status_text = "已付款";
              data[i].pay_status_code = 0;
            } else {
              data[i].pay_status_text = "未付款";
              data[i].pay_status_code = 1;
            }
          }
        }
        $scope.orderListTableData = data;

        $scope.tableParams = new NgTableParams({ filter: { order_id: orderId }, }, { dataset: data });

      }
    });
  }


  $scope.deleteOrder = function (order) {
    orderService.deleteOrder(order.order_id).then(function (data) {
      if (data.code && data.code != 200) {
        notifyWarning(data.msg)
      } else {
        notifyInfo(data.msg);
        $scope.getOrderList();
      }
    });
  }


  $scope.orderPopup = function (data) {
    var modalInstance = $uibModal.open({
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'orderDetailModalCotent.html',
      controller: 'adminModalInstanceCtrl',
      controllerAs: '$scope',
      size: "lg",
      appendTo: "",
      resolve: {
        page: function () {
          return 'order';
        },
        title: function () {
          return null;
        },
        message: function () {
          return null;
        },
        orderId: function () {
          return data.order_id;;
        }
      }
    });
  }

  $scope.updateStatus = function (orderid) {

    for (i in $scope.orderListTableData) {
      if ($scope.orderListTableData[i].order_id == orderid) {
        if ($scope.orderListTableData[i].status == 1)
          $scope.orderListTableData[i].status = 0;
        else
          $scope.orderListTableData[i].status = 1;
        orderService.updateOrderStatus($scope.orderListTableData[i].id, $scope.orderListTableData[i].status).then(function (data) {
          if (data.code && data.code != 200) {
            notifyWarning(data.msg)
          } else {
            notifyInfo(data.msg);
          }
        });
      }
    }


  }

  // $scope.updateStatus = function (i) {
  //   if ($scope.orderListTableData[i].status == 1)
  //     $scope.orderListTableData[i].status = 0;
  //   else
  //     $scope.orderListTableData[i].status = 1;
  //   orderService.updateOrderStatus($scope.orderListTableData[i].id, $scope.orderListTableData[i].status).then(function (data) {
  //     if (data.code && data.code != 200) {
  //       notifyWarning(data.msg)
  //     } else {
  //       notifyInfo(data.msg);
  //     }
  //   });

  // }

  /*** Product *****/
  $scope.newProductMenu = {};

  $scope.getProductList = function () {
    productService.getProduct('').then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else {
        $scope.orderListTableData = data;
        $scope.tableParams = new NgTableParams({ count: 100 }, { dataset: data });

      }
    });
  }

  $scope.updateMenuFile = function () {
    productService.updateMenu($scope.newProductMenu).then(function (data) {
      if (data.code && data.code != 200) {
        notifyWarning(data.msg)
      } else {
        notifyInfo(data.msg);
      }
    });
  }


  $scope.getDistinctProductType = function () {
    $scope.productTypeMap = new Map();
    $scope.typeTable = [];

    productService.getProductType().then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else {
        $scope.typeTable = data;
        for (let i = 0; i < data.length; i++) {
          let type = data[i].type;
          if (!$scope.productTypeMap.get(type)) {
            $scope.productTypeMap.set(type, data[i].type_order);
          }
        }
      }
    });
  }
  $scope.deleteProduct = function (product) {
    productService.deleteProduct(product.id).then(function (data) {
      if (data.code && data.code != 200) {
        notifyWarning(data.msg)
      } else {
        notifyInfo(data.msg);
        $scope.getProductList();
      }
    });
  }
  $scope.updateProductOrder = function (product) {
    productService.updateProductOrder(product.id, product.product_order).then(function (data) {
      if (data.code && data.code != 200) {
        notifyWarning(data.msg)
      } else {
        notifyInfo(data.msg);
      }
    });
  }
  $scope.updateProductTypeOrder = function (product) {
    productService.updateProductTypeOrder(product.type_order, product.type).then(function (data) {
      if (data.code && data.code != 200) {
        notifyWarning(data.msg)
      } else {
        notifyInfo(data.msg);
      }
    });
  }

  $scope.infoPopup = function (title, message) {
    $uibModal.open({
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'infoModalCotent.html',
      controller: 'adminModalInstanceCtrl',
      controllerAs: '$scope',
      size: "lg",
      appendTo: "",
      resolve: {
        page: function () {
          return 'info';
        },
        title: function () {
          return title;
        },
        message: function () {
          return message;
        },
        orderId: function () {
          return null;
        }
      }
    });
  }
  $scope.addProductPopup = function () {
    let modalInstance = $uibModal.open({
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'addProductModalCotent.html',
      controller: 'adminModalInstanceCtrl',
      controllerAs: '$scope',
      size: "lg",
      appendTo: "",
      resolve: {
        page: function () {
          return 'product';
        },
        title: function () {
          return '商品新增';
        },
        message: function () {
          return "";
        },
        orderId: function () {
          return null;
        }
      }
    });
    modalInstance.result.then(function () {
    }, function () {
      $scope.getProductList();
    });
  }
  $scope.editProductPopup = function (data) {
    let modalInstance = $uibModal.open({
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'editProductModalCotent.html',
      controller: 'adminModalInstanceCtrl',
      controllerAs: '$scope',
      size: "lg",
      appendTo: "",
      resolve: {
        page: function () {
          return 'product';
        },
        title: function () {
          return '商品編輯';
        },
        message: function () {
          return data;
        },
        orderId: function () {
          return null;
        }
      }
    });

    modalInstance.result.then(function () {
    }, function () {
      $scope.getProductList();
    });

  }

  $scope.updateFrozen = function (id, isFrozen) {

    productService.updateFrozen(id, !isFrozen).then(function (data) {
      if (data.code && data.code != 200) {
        notifyWarning(data.msg)
      } else {
        notifyInfo(data.msg);
      }
    });

  }
  /************Rule*************** */
  $scope.ruleData = [];
  $scope.getRules = function () {
    rulesService.getRules('').then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else {
        $scope.ruleData = data;
      }
    });
  }

  $scope.updateNotice = function (editedNotice) {
    rulesService.updateNotice(editedNotice).then(function (data) {
      if (data.code && data.code != 200) {
        notifyWarning(data.msg)
      } else {
        notifyInfo(data.msg);
      }
    });
  }
  /********NEWS********** */
  $scope.newsTable = [];
  $scope.newMessage = "";
  $scope.newTitle = "";
  $scope.newDate = new Date();

  $scope.getNews = function () {
    newsService.getNews().then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else {
        $scope.newsTable = new NgTableParams({}, { dataset: data });

      }
    });

  }
  $scope.deleteNews = function (newsId) {
    newsService.deleteNews(newsId).then(function (data) {
      if (data.code && data.code != 200) {
        notifyWarning(data.msg)
      } else {
        notifyInfo(data.msg);
        $scope.getNews();
      }
    });
  }

  $scope.addNews = function () {
    if ($scope.newTitle == "" || $scope.newMessage == "" || $scope.newDate == "") {
      notifyWarning("請完整填完資訊");
      return;
    }
    let month = '' + ($scope.newDate.getMonth() + 1);
    let day = '' + $scope.newDate.getDate();
    let year = $scope.newDate.getFullYear();
    let newsJson = {
      title: $scope.newTitle,
      message: $scope.newMessage,
      date: month + "/" + day + "/" + year,
      link: $scope.newLink,
      show: $scope.newShow
    }


    newsService.addNews(newsJson).then(function (data) {
      if (data.code && data.code != 200) {
        notifyWarning(data.msg)
      } else {
        notifyInfo(data.msg);
        $scope.getNews();
      }
    });

  }

  $scope.updateNewsShow = function (id, status) {
    newsService.updateNewsShow(id, !status).then(function (data) {
      if (data.code && data.code != 200) {
        notifyWarning(data.msg)
      } else {
        notifyInfo(data.msg);
      }
    });
  }
  $scope.editNewsPopup = function (data) {
    data.newDate = new Date(data.date);
    let modalInstance = $uibModal.open({
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'editNewsModalCotent.html',
      controller: 'adminModalInstanceCtrl',
      controllerAs: '$scope',
      size: "lg",
      appendTo: "",
      resolve: {
        page: function () {
          return 'product';
        },
        title: function () {
          return '商品編輯';
        },
        message: function () {
          return data;
        },
        orderId: function () {
          return null;
        }
      }
    });

    modalInstance.result.then(function () {
    }, function () {
      $scope.getNews();
    });
  }

  /************Basic info ** */
  $scope.infoData = [];
  $scope.getInfo = function () {
    infoService.getInfo().then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else {
        $scope.infoData = data;
      }
    });
  }
  $scope.updateInfo = function (infoData) {
    infoService.updateInfo(infoData).then(function (data) {
      if (data.code && data.code != 200) {
        notifyWarning(data.msg)
      } else {
        notifyInfo(data.msg);
      }
    });
  }
  /**************** Comment ****************/
  $scope.commentList = [];
  $scope.getComment = function () {
    contactService.getComment().then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else {
        $scope.commentList = data;
      }
    });
  }
  /**************** User ****************/
  $scope.userList = [];
  $scope.creditRule = {};
  $scope.getUser = function () {
    userService.getUserList().then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else {
        $scope.tableParams = new NgTableParams({}, { dataset: data });

      }
    });
  }
  $scope.getCredits = function () {
    userService.getCredits().then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else {
        $scope.creditRule = data[0];
      }
    });
  }

  $scope.updateBirthdayCredit = function (credit) {

    credit.birthday_credit_enable = credit.birthday_credit_enable == true ? 0 : 1;
    userService.updateCredits(credit).then(function (data) {
      if (data.code && data.code != 200) {
        notifyWarning(data.msg)
      } else {
        notifyInfo(data.msg);
      }
    });
  }
  /***************媒體********* */
  $scope.mediaTable = [];
  $scope.getMedia = function () {
    mediaService.getMedia().then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else {
        for (let i = 0; i < data.length; i++) {
          if (data[i].show2 == 0) {
            data[i].showVal = false;
          } else {
            data[i].showVal = true;
          }
        }
        $scope.mediaTable = new NgTableParams({}, { dataset: data });
      }
    });
  }

  $scope.editMediaPopup = function (data) {
    let modalInstance = $uibModal.open({
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'editMediaModalCotent.html',
      controller: 'adminMediaModalInstanceCtrl',
      controllerAs: '$scope',
      size: "lg",
      appendTo: "",
      resolve: {
        page: function () {
          return 'media';
        },
        title: function () {
          return '媒體編輯';
        },
        message: function () {
          return data;
        },
        orderId: function () {
          return null;
        }
      }
    });

    modalInstance.result.then(function () {
    }, function () {
      $scope.getMedia();
    });
  }
  $scope.deleteMedia = function (id) {
    mediaService.deleteMedia(id).then(function (data) {
      if (data.code && data.code != 200) {
        notifyWarning(data.msg)
      } else {
        notifyInfo(data.msg);
        $scope.getMedia();
      }
    });
  }

  /***************食譜********* */
  $scope.recipeTable = [];
  $scope.numberOfRecipe = 0;
  $scope.getRecipe = function () {
    recipeService.getRecipe().then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else {
        $scope.numberOfRecipe = data.length;
        $scope.recipeTable = new NgTableParams({}, { dataset: data });
      }
    });
  }
  $scope.deleteRecipe = function (recipeId) {
    recipeService.deleteRecipe(recipeId).then(function (data) {
      if (data.code && data.code != 200) {
        notifyWarning(data.msg)
      } else {
        notifyInfo(data.msg);
        $scope.getRecipe();
      }
    });
  }
  $scope.addMediaPopup = function () {
    let modalInstance = $uibModal.open({
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'addMediaModalCotent.html',
      controller: 'adminMediaAddModalInstanceCtrl',
      controllerAs: '$scope',
      size: "lg",
      appendTo: "",
      resolve: {
        page: function () {
          return 'product';
        },
        title: function () {
          return '商品新增';
        },
        message: function () {
          return "";
        },
        orderId: function () {
          return null;
        }
      }
    });
    modalInstance.result.then(function () {
    }, function () {
      $scope.getMedia();
    });
  }

  $scope.addRecipePopup = function () {
    let modalInstance = $uibModal.open({
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'addRecipeModalCotent.html',
      controller: 'adminModalInstanceCtrl',
      controllerAs: '$scope',
      size: "lg",
      appendTo: "",
      resolve: {
        page: function () {
          return 'product';
        },
        title: function () {
          return '商品新增';
        },
        message: function () {
          return "";
        },
        orderId: function () {
          return null;
        }
      }
    });
    modalInstance.result.then(function () {
    }, function () {
      location.reload();
    });
  }
  $scope.editRecipePopup = function (data) {
    let modalInstance = $uibModal.open({
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'editRecipeModalCotent.html',
      controller: 'adminModalInstanceCtrl',
      controllerAs: '$scope',
      size: "lg",
      appendTo: "",
      resolve: {
        page: function () {
          return 'product';
        },
        title: function () {
          return '商品編輯';
        },
        message: function () {
          return data;
        },
        orderId: function () {
          return null;
        }
      }
    });

    modalInstance.result.then(function () {
    }, function () {
      $scope.getRecipe();
    });

  }


  /***************社群********* */
  $scope.socialDataTable = [];
  $scope.getSocial = function () {
    socialService.getSocial().then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else {
        for (let i = 0; i < data.length; i++) {
          if (data[i].show == 0) {
            data[i].showVal = false;
          } else {
            data[i].showVal = true;
          }
        }
        $scope.socialDataTable = new NgTableParams({}, { dataset: data });
      }
    });
  }


  $scope.updateSocial = function () {
    let data = $scope.socialDataTable.data;
    for (let i = 0; i < data.length; i++) {
      if (data[i].showVal == false) {
        data[i].show = 0;
      } else {
        data[i].show = 1;
      }
    }
    socialService.updateSocial(data).then(function (data) {
      if (data.code && data.code != 200) {
        notifyWarning(data.msg)
      } else {
        notifyInfo(data.msg);
      }
    });
  }
  /****************廣告*****************/
  $scope.bannerTable = [];
  $scope.bannerImageDesktopSrc1 = null;
  $scope.bannerImageDesktopSrc2 = null;
  $scope.bannerImageMobileSrc1 = null;
  $scope.bannerImageMobileSrc2 = null;

  $scope.getBanner = function () {
    bannerService.getBanner().then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else {
        for (let i = 0; i < data.length; i++) {
          if (data[i].show == 0) {
            data[i].showVal = false;
          } else {
            data[i].showVal = true;
          }
        }
        $scope.bannerTable = data[0];
      }
    });
  }

  $scope.updateBanner = function () {
    $scope.bannerTable.bannerImageDesktopSrc1 = $scope.bannerImageDesktopSrc1;
    $scope.bannerTable.bannerImageMobileSrc1 = $scope.bannerImageMobileSrc1;
    $scope.bannerTable.bannerImageDesktopSrc2 = $scope.bannerImageDesktopSrc2;
    $scope.bannerTable.bannerImageMobileSrc2 = $scope.bannerImageMobileSrc2;

    bannerService.updateBanner($scope.bannerTable).then(function (data) {
      if (data.code && data.code != 200) {
        notifyWarning(data.msg)
      } else if (data.status == 413) {
        notifyWarning("上傳圖片太大，請勿一次上傳總共超過8MB的圖片。")
      } else {
        notifyInfo(data.msg);
      }
    });
  }

  $scope.init = function (page) {
    $scope.sessionCheck();
    if (page == 'order')
      $scope.getOrderList();
    else if (page == 'product') {
      $scope.getProductList();
    } else if (page == 'rule')
      $scope.getRules();
    else if (page == 'comment')
      $scope.getComment();
    else if (page == 'user')
      $scope.getUser();
    else if (page == 'news')
      $scope.getNews();
    else if (page == 'recipe')
      $scope.getRecipe();
    else if (page == 'social')
      $scope.getSocial();
    else if (page == 'media')
      $scope.getMedia();
    else if (page == 'banner')
      $scope.getBanner();
    else if (page == 'product_order') {
      $scope.getProductList();
      $scope.getDistinctProductType();
    }
    else
      $scope.getInfo();

    $rootScope.loadingPage = false;
  }

});
app.controller('adminMediaModalInstanceCtrl', function ($scope, $uibModalInstance, mediaService, page, title, message) {
  $scope.title = title;
  $scope.message = message;
  $scope.errorMsg = '';
  $scope.successMsg = '';
  $scope.imageSrc = null;
  $scope.imageSrc2 = null;
  $scope.imageSrc3 = null;
  $scope.imageSrc4 = null;
  $scope.imageSrc5 = null;
  $scope.imageSrc6 = null;
  $scope.imageSrc7 = null;
  $scope.imageSrc8 = null;
  $scope.imageSrc9 = null;
  $scope.imageSrc10 = null;
  $scope.imageSrc11 = null;
  $scope.imageSrc12 = null;
  $scope.imageSrc13 = null;
  $scope.imageSrc14 = null;
  $scope.imageSrc15 = null;

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
  $scope.updateMedia = function (data) {
    data.newImg = $scope.imageSrc;
    data.newImg2 = $scope.imageSrc2;
    data.newImg3 = $scope.imageSrc3;
    data.newImg4 = $scope.imageSrc4;
    data.newImg5 = $scope.imageSrc5;
    data.newImg6 = $scope.imageSrc6;
    data.newImg7 = $scope.imageSrc7;
    data.newImg8 = $scope.imageSrc8;
    data.newImg9 = $scope.imageSrc9;
    data.newImg10 = $scope.imageSrc10;
    data.newImg11 = $scope.imageSrc11;
    data.newImg12 = $scope.imageSrc12;
    data.newImg13 = $scope.imageSrc13;
    data.newImg14 = $scope.imageSrc14;
    data.newImg15 = $scope.imageSrc15;
    if (data.showVal == false) {
      data.show2 = 0;
    } else {
      data.show2 = 1;
    }
    mediaService.updateMedia(data).then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else if (data.status == 413) {
        $scope.errorMsg = "上傳圖片太大，請勿一次上傳總共超過5MB的圖片。"
      } else {
        $scope.successMsg = (data.msg)
      }
    });
  }

  $scope.updateProduct = function (data) {
    data.newImg = $scope.imageSrc;
    productService.updateProduct(data).then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else if (data.status == 413) {
        $scope.errorMsg = "上傳圖片太大，請使用5MB以下的圖片。"
      } else {
        $scope.successMsg = (data.msg)
      }
    });
  }

  $scope.deleteImg = function (index) {
    $scope.message.img2 = null;

  }
  $scope.$on("fileProgress", function (e, progress) {
    $scope.progress = progress.loaded / progress.total;
  });

});
app.controller('adminMediaAddModalInstanceCtrl', function ($scope, $uibModalInstance, mediaService, page, title, message) {
  $scope.title = title;
  $scope.message = {
    title: "",
    subtitle: "",
    description: "",
    img1: null,
    img2: null,
    img3: null,
    img4: null,
    img5: null,
    img6: null,
    img7: null,
    img8: null,
    img9: null,
    img10: null,
    img11: null,
    img12: null,
    img13: null,
    img14: null,
    img15: null,
    show2: false,
    showVal: false
  };
  $scope.errorMsg = '';
  $scope.successMsg = '';
  $scope.imageSrc = null;
  $scope.imageSrc2 = null;
  $scope.imageSrc3 = null;
  $scope.imageSrc4 = null;
  $scope.imageSrc5 = null;
  $scope.imageSrc6 = null;
  $scope.imageSrc7 = null;
  $scope.imageSrc8 = null;
  $scope.imageSrc9 = null;
  $scope.imageSrc10 = null;
  $scope.imageSrc11 = null;
  $scope.imageSrc12 = null;
  $scope.imageSrc13 = null;
  $scope.imageSrc14 = null;
  $scope.imageSrc15 = null;

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
  $scope.addMedia = function (data) {
    console.log(data)
    data.newImg = $scope.imageSrc;
    data.newImg2 = $scope.imageSrc2;
    data.newImg3 = $scope.imageSrc3;
    data.newImg4 = $scope.imageSrc4;
    data.newImg5 = $scope.imageSrc5;
    data.newImg6 = $scope.imageSrc6;
    data.newImg7 = $scope.imageSrc7;
    data.newImg8 = $scope.imageSrc8;
    data.newImg9 = $scope.imageSrc9;
    data.newImg10 = $scope.imageSrc10;
    data.newImg11 = $scope.imageSrc11;
    data.newImg12 = $scope.imageSrc12;
    data.newImg13 = $scope.imageSrc13;
    data.newImg14 = $scope.imageSrc14;
    data.newImg15 = $scope.imageSrc15;
    if (data.showVal == false) {
      data.show2 = 0;
    } else {
      data.show2 = 1;
    }
    mediaService.addMedia(data).then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else if (data.status == 413) {
        $scope.errorMsg = "上傳圖片太大，請勿一次上傳總共超過5MB的圖片。"
      } else {
        $scope.successMsg = (data.msg)
      }
    }).catch(function (data) {
      console.log(data);
      alert("系統錯誤")
    });
  }
  $scope.updateMedia = function (data) {
    data.newImg = $scope.imageSrc;
    data.newImg2 = $scope.imageSrc2;
    data.newImg3 = $scope.imageSrc3;
    data.newImg4 = $scope.imageSrc4;
    data.newImg5 = $scope.imageSrc5;
    data.newImg6 = $scope.imageSrc6;
    data.newImg7 = $scope.imageSrc7;
    data.newImg8 = $scope.imageSrc8;
    data.newImg9 = $scope.imageSrc9;
    data.newImg10 = $scope.imageSrc10;
    data.newImg11 = $scope.imageSrc11;
    data.newImg12 = $scope.imageSrc12;
    data.newImg13 = $scope.imageSrc13;
    data.newImg14 = $scope.imageSrc14;
    data.newImg15 = $scope.imageSrc15;
    if (data.showVal == false) {
      data.show2 = 0;
    } else {
      data.show2 = 1;
    }
    mediaService.updateMedia(data).then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else if (data.status == 413) {
        $scope.errorMsg = "上傳圖片太大，請勿一次上傳總共超過5MB的圖片。"
      } else {
        $scope.successMsg = (data.msg)
      }
    });
  }

  $scope.updateProduct = function (data) {
    data.newImg = $scope.imageSrc;
    productService.updateProduct(data).then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else if (data.status == 413) {
        $scope.errorMsg = "上傳圖片太大，請使用5MB以下的圖片。"
      } else {
        $scope.successMsg = (data.msg)
      }
    });
  }

  $scope.deleteImg = function (index) {
    $scope.message.img2 = null;

  }
  $scope.$on("fileProgress", function (e, progress) {
    $scope.progress = progress.loaded / progress.total;
  });

});
app.controller('adminModalInstanceCtrl', function ($scope, $uibModalInstance, orderService, orderId, productService, newsService, recipeService, page, title, message) {
  $scope.title = title;
  $scope.message = message;
  $scope.errorMsg = '';
  $scope.successMsg = '';
  $scope.imageSrc = null;
  $scope.newProduct = {};
  $scope.newRecipe = {};
  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
  $scope.getOrderOnId = function (orderId) {
    orderService.getOrderOnId(orderId).then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else {
        $scope.orderListTableData = data;
      }
    });
  }
  $scope.updateNews = function (news) {
    let month = '' + (news.newDate.getMonth() + 1);
    let day = '' + news.newDate.getDate();
    let year = news.newDate.getFullYear();
    let newsJson = {
      title: news.title,
      message: news.message,
      date: month + "/" + day + "/" + year,
      id: news.id,
      link: news.link
    }
    newsService.updateNews(newsJson).then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else {
        $scope.successMsg = (data.msg)
      }
    });
  }
  $scope.updateRecipe = function (data) {
    data.newImg = $scope.imageSrc;
    recipeService.updateRecipe(data).then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else if (data.status == 413) {
        $scope.errorMsg = "上傳圖片太大，請使用5MB以下的圖片。"
      } else {
        $scope.successMsg = (data.msg)
      }
    });
  }
  $scope.addRecipe = function (data) {
    data.newImg = $scope.imageSrc;
    recipeService.addRecipe(data).then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else {
        $scope.successMsg = (data.msg)
      }
    });
  }
  $scope.updateProduct = function (data) {
    data.newImg = $scope.imageSrc;
    productService.updateProduct(data).then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else if (data.status == 413) {
        $scope.errorMsg = "上傳圖片太大，請使用5MB以下的圖片。"
      } else {
        $scope.successMsg = (data.msg)
      }
    });
  }

  $scope.addProduct = function (data) {
    data.newImg = $scope.imageSrc;
    productService.addProduct(data).then(function (data) {
      console.log(data);
      if (data.status && data.status != 200) {
        if (data.status == 413) {
          $scope.errorMsg = "上傳圖片太大，請使用5MB以下的圖片。"
        } else {
          $scope.errorMsg = data.statusText;
        }

      }
      if (data.code && data.code != 200) {
        alert(data.msg)
        $scope.errorMsg = data.msg;
      } else {
        $scope.successMsg = (data.msg)
      }
    });
  }

  $scope.$on("fileProgress", function (e, progress) {
    $scope.progress = progress.loaded / progress.total;
  });


  $scope.init = function () {
    if (page == 'order')
      $scope.getOrderOnId(orderId);

  }
  $scope.init();
});