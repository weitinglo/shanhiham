
app.controller('homeController', function ($scope, productService,socialService,bannerService,$rootScope) {
  $rootScope.loadingPage = true;

  updateMenu('home');
  $scope.loading=true;

  $scope.type = '';
  $scope.productTableList = [];
  $scope.socialData = [];
  $scope.bannerTable = [];
  $scope.goProduct = function(){
    location.href="#!product";
  }
  $scope.getProduct = function () {
    productService.getProduct($scope.type).then(function (data) {
      if (data.code && data.code != 200) {
        //nowuiDashboard.showErrorNotification('top', 'center', data.msg)
      } else {
        for (let i = 0; i < 3; i++) {
          if(data[i].d_price=="" || data[i].d_price==null)
            data[i].d_price="";
          $scope.productTableList.push(data[i]);
        };
      }
    });
  }
    
  $scope.getBanner = function(){
    bannerService.getBanner().then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else {
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
          $('#first-background-img').attr('src', 'images/home/home-background2.jpg');
          $(".slides-container").css("top","110px")
        } else {
          $('#first-background-img').attr('src', 'images/home/home-background.jpg');
        }
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
          $scope.bannerTable.img1 = data[0].mobile1;
          $scope.bannerTable.img2 = data[0].mobile2;
        }else{
          $scope.bannerTable.img1 = data[0].desktop1;
          $scope.bannerTable.img2 = data[0].desktop2;
        }       
       
      }
    });
  }

  $scope.facebookJson = {};
  $scope.instagramJson = {};
  $scope.lineJson = {};
  $scope.foodPandaJson = {};
  $scope.ecpayJson = {};

  $scope.getSocial = function(){
    socialService.getSocial().then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else {
        for(let i=0;i<data.length;i++){
          if(data[i].name=='Facebook'){
            $scope.facebookJson={
              'link':data[i].link,
              'show':data[i].show
            };
          }else if(data[i].name=='Instagram'){
            $scope.instagramJson={
              'link':data[i].link,
              'show':data[i].show
            };
          }else if(data[i].name=='Line'){
            $scope.lineJson={
              'link':data[i].link,
              'show':data[i].show
            };
          }else if(data[i].name=='Foodpanda'){
            $scope.foodPandaJson={
              'link':data[i].link,
              'show':data[i].show
            };
          }else if(data[i].name=="綠界"){
            $scope.ecpayJson={
              'link':data[i].link,
              'show':data[i].show
            };
          }
        }
        $scope.socialData = data;

      }
    });

  }
  $scope.doneloading=false;
  $scope.getProduct();
  $scope.getBanner();
  $scope.getSocial();

  $scope.$on('$viewContentLoaded', function(){
    setTimeout(function(){   
      $('#slides').superslides({
        inherit_height_from: '.cover-slides',
        inherit_width_from: '.cover-slides',
        play: false,
        animation: 'fade',
      });
      
    }, 500);
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){

    }else{
      setTimeout(function(){   
        $('.sub-background-img').css("height","100%");
        $('.sub-background-img').css("top","-15px");
      }, 1000);
    }
    $rootScope.loadingPage=false;
    setTimeout(function(){   
      $("#first-background-img").css("opacity","1")
    }, 1600);
    
    
  });
});

