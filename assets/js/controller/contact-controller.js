
app.controller('contactController', function ($scope, contactService, infoService,$rootScope) {
  $rootScope.loadingPage = true;

  updateMenu('contact');
  $scope.successMsg = '';
  $scope.errorMsg = '';
  $scope.commentName = '';
  $scope.commentEmail = '';
  $scope.commentPhone = '';
  $scope.commentMsg = '';

  $scope.infoData = [];
  $scope.getInfo = function () {
    infoService.getInfo().then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else {
        $scope.infoData = data;
      }
      $rootScope.loadingPage = false;
    });
  }

  $scope.sendComment = function () {
    let dataJson = {
      name: $scope.commentName,
      email: $scope.commentEmail,
      phone: $scope.commentPhone,
      message: $scope.commentMsg
    }
    contactService.sendComment(dataJson).then(function (data) {
      if (data.code && data.code != 200) {
        $scope.errorMsg = data.msg;
      } else {
        $scope.successMsg = data.msg;
      }
    });
  }
  $scope.init = function () {
    $scope.getInfo();
  }
  $scope.init();
});
