
app.controller('modalInstanceCtrl', function ($scope, $uibModalInstance, items) {

    $scope.items = items;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $uibModalInstance.rendered.then(function () {
        $('#media-slides').superslides({
            inherit_width_from: '.slides-container',
            inherit_height_from: '.slides-container',
            play: 5000,
            animation: 'fade',
        });
    });

});
app.controller('mediaController', function ($scope, $uibModal,mediaService,$rootScope) {
    $rootScope.loadingPage = true;

    updateMenu('media');
    $scope.mediaTableData=[];
    $scope.getMedia = function(){
        mediaService.getMedia().then(function (data) {
            if (data.code && data.code != 200) {
                //nowuiDashboard.showErrorNotification('top', 'center', data.msg)
            } else {
               
                $scope.mediaTableData = data;
            }
            $rootScope.loadingPage = false;
        });
    }
    $scope.getMedia();
    $scope.popup = function (type) {
        if (type == 1) {
            $scope.items = ['/images/media/m1/1.jpg', '/images/media/m1/2.jpg', '/images/media/m1/3.jpg', '/images/media/m1/4.jpg'];
        }else if (type == 2) {
            $scope.items = ['/images/media/m3/1.jpg', '/images/media/m3/2.jpg', '/images/media/m3/3.jpg'];
        }else if (type == 3) {
            $scope.items = ['/images/media/m4/1.jpg', '/images/media/m4/2.jpg'];
        }else if (type == 4) {
            $scope.items = ['/images/media/m2/r01.jpg', '/images/media/m2/r02.jpg', '/images/media/m2/r03.jpg', '/images/media/m2/r04.jpg','/images/media/m2/r05.jpg','/images/media/m2/r06.jpg','/images/media/m2/r07.jpg','/images/media/m2/r08.jpg','/images/media/m2/r09.jpg','/images/media/m2/r10.jpg','/images/media/m2/r11.jpg'];
        }else if (type == 5) {
            $scope.items = ['/images/media/m5/1.jpg'];
        }else if (type == 6) {
            $scope.items = ['/images/media/m6/1.jpg','/images/media/m6/2.jpg','/images/media/m6/3.jpg','/images/media/m6/4.jpg'];
        }
        var modalInstance = $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'modalCotent.html',
            controller: 'modalInstanceCtrl',
            controllerAs: '$scope',
            size: "lg",
            appendTo: "",
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });
    }
});
