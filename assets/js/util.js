function updateMenu(menu) {
  let menuStatus = $("header .collapse").collapse('hide');
  $("#menuHome").removeClass("active");
  $("#menuContact").removeClass("active");
  $("#menuLocation").removeClass("active");
  $("#menuMedia").removeClass("active");
  $("#menuProduct").removeClass("active");
  $("#menuRecipe").removeClass("active");
  $("#menuAdmin").removeClass("active");
  $("#menuProfile").removeClass("active");
  switch (menu) {
    case 'home':
      $("#menuHome").addClass("active");
      break;
    case 'contact':
      $("#menuContact").addClass("active");
      break;
    case 'location':
      $("#menuLocation").addClass("active");
      break;
    case 'media':
      $("#menuMedia").addClass("active");
      break;
    case 'product':
      $("#menuProduct").addClass("active");
      break;
    case 'recipe':
      $("#menuRecipe").addClass("active");
      break;
    case 'admin':
      $("#menuAdmin").addClass("active");
      break;
    case 'profile':
      $("#menuProfile").addClass("active");
      break;

  }
  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    setTimeout(function(){   $(this).scrollTop(0);
    }, 500);
  }else{
    setTimeout(function(){   $(this).scrollTop(0);
    }, 400);
  }


}

function isIEcheck() {

  var ua = window.navigator.userAgent;
  var msie = ua.indexOf("MSIE ");

  if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
  {
      return true
  }
  else  // If another browser, return 0
  {
    return false;

  }
}

function formatBirthdayDate(date) {
  var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2)
    month = '0' + month;
  if (day.length < 2)
    day = '0' + day;

  return [year, month, day].join('-');
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function saveProductToCookie(product) {
  var savedItem = $.cookie("product");
  if (savedItem == "" || savedItem == null) {
    let productArr = [];
    productArr.push(product);
    $.cookie("product", JSON.stringify(productArr));
  } else {
    savedItem = JSON.parse(savedItem)
    let updated = false;
    for (i in savedItem) {
      let item = savedItem[i];
      if (item.name == product.name) {
        savedItem[i].amout += product.amout;
        updated = true;
      }
    }
    if (!updated)
      savedItem.push(product)
    $.cookie("product", JSON.stringify(savedItem));
  }
}

function getProductinCookie() {
  let savedItem = $.cookie("product");
  if (savedItem)
    return JSON.parse(savedItem);
  else
    return null;
}

function updateProductCountCookie(data) {
  try {
    let productJson = {
      id: data.id,
      name: data.name,
      img: data.img,
      price: data.d_price,
      amount: data.amount
    }
    savedItems = getProductinCookie();
    let product_cookie_arr = [];
    if (!savedItems) {
      product_cookie_arr.push(productJson);
      $.cookie("product", JSON.stringify(product_cookie_arr));
    } else {
      for (i in savedItems) {
        if (savedItems[i].id == data.id)
          savedItems[i].amount = data.amount;
      }

      $.cookie("product", JSON.stringify(savedItems));
    }
    return true;
  } catch (err) {
    return false;
  }
}
function deleteDataFromProductCookie(data) {
  savedItems = getProductinCookie();
  for (i in savedItems) {
    if (savedItems[i].id == data.id)
      savedItems.splice(i, 1);
  }
  console.log(savedItems);
  $.cookie("product", JSON.stringify(savedItems));

}
function notifyNews(message) {
  let url ="";
  let link = message.link;
  if(link){
      if(!link.includes("http")){
        url="http://"+link;
      }else{
        url=link;
      }
  }

  $.notify({
    // options
    icon: 'fas fa-bullhorn',
    title: message.title+"-"+message.date,
    message: message.message,
    url: url,
    target: '_blank'
  }, {
    // settings
    newest_on_top: true,
    type: 'primary',
    timer: 3000,
    allow_dismiss: true,
    animate: {
      enter: 'animated fadeInDown',
      exit: 'animated fadeOutUp'
    },
    offset: {
      x: 50,
      y: 100
    },
    spacing: 10,
    placement: {
      from: "top",
      align: "left"
    },
    template: '<div data-notify="container"  class="col-xs-9 col-sm-3 alert alert-{0}" role="alert">' +
      '<button type="button" aria-hidden="true" class="close" data-notify="dismiss" style="font-size:22px; padding-bottom:25px; width:55px;"><i class="fas fa-times"></i></button>' +
      '<span data-notify="icon"></span> ' +
      '<span data-notify="title" style="font-size:20px;">{1}</span><br> ' +
      '<span data-notify="message" style="padding-right: 40px; display:block">{2}</span>' +
      '<div class="progress" data-notify="progressbar">' +
      '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
      '</div>' +
      '<a href="{3}" target="{4}" data-notify="url"></a>' +
      '</div>'
  });
}

function notifyInfo(message) {
  $.notify({
    // options
    message: message
  }, {
    // settings
    type: 'info',
    timer: 1000,
    allow_dismiss: true,
    animate: {
      enter: 'animated fadeInDown',
      exit: 'animated fadeOutUp'
    },
    placement: {
      from: "bottom",
      align: "right"
    }
  });
}

function notifyWarning(message) {
  $.notify({
    // options
    message: message
  }, {
    // settings
    type: 'warning',
    timer: 1000,
    allow_dismiss: true,
    animate: {
      enter: 'animated fadeInDown',
      exit: 'animated fadeOutUp'
    },
    placement: {
      from: "bottom",
      align: "right"
    }
  });
}


var loadFile = function (event) {
  var image = document.getElementById('output');
  image.src = URL.createObjectURL(event.target.files[0]);
};

function getUrlParameter(sParam) {
  var sPageURL = window.location.search.substring(1),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;

  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');

    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
    }
  }
};